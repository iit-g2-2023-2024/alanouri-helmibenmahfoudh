


import React from 'react';
import { View, StyleSheet } from 'react-native';
import Card from './components/Card';
import CardImage from './components/CardImage';
import {default as APP_STORYBOOK} from './.storybook'; 
import Constants from 'expo-constants';

console.log(Constants)
const App: React.FC = () => {
  return (
    <View style={styles.container}>
      {/* Column 1: Cards */}
      <View style={styles.column}>
        {/* Vertical Card */}
        <Card
          title="Vertical Card"
          price="$20"
          discount="$15"
          percentage={25}
          imageUrl="meuble.png"
          rating={4.5}
          onPress={() => console.log('Card Pressed!')}
          orientation="vertical"
        />

        {/* Horizontal Card */}
        <Card
          title="Horizontal Card"
          price="$25"
          discount="$18"
          percentage={28}
          imageUrl="meuble.png"
          rating={4.8}
          onPress={() => console.log('Card Pressed!')}
          orientation="horizontal"
        />
      </View>

      {/* Column 2: Card Images */}
      <View style={styles.column}>
        <CardImage
          title="Title"
          subtitle="Bbb"
          onPress={() => console.log('Card Pressed!')}
          orientation="horizontal"
        />
        <CardImage
          title="Title"
          subtitle="bbb"
          onPress={() => console.log('Card Pressed!')}
          orientation="vertical"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row', // Two columns
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5fcff',
  },
  column: {
    flex: 1, // Each column takes equal space
  },
});
let APP_FINAL = null;
if(Constants.expoConfig?.extra?.storybookEnabled !== "true"){
  APP_FINAL = App
}
else {
  APP_FINAL = APP_STORYBOOK
}

export default APP_FINAL;




