import { combineReducers, createStore } from 'redux';
import productReducer from './reducer/product';
import notifReducer from './reducer/notification';
import typeReducer from './reducer/type';

const initialState = {};

const rootReducer = combineReducers({
  product : productReducer,
  notif : notifReducer,
  type : typeReducer
})

const store = createStore(rootReducer);

export default store;