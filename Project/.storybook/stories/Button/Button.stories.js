import React from 'react';
import { View } from 'react-native';
import { MyButton } from './Button';

const MyButtonMeta = {
  title: 'MyButton Test',
  component: MyButton,
  argTypes: {
    onPress: { action: 'pressed the button' },
  },
  args: {
    text: 'Hello world iit',
  },
  decorators: [
    (Story) => (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Story />
      </View>
    ),
  ],
};

export default MyButtonMeta;

export const BasicTest = {};

export const AnotherExampleTest = {
  args: {
    text: 'Another example 2023',
  },
};
