import React from 'react';
import { View } from 'react-native';
import  MyCard  from '../../../components/Card';

const MyCardMeta = {
  title: 'MyCard Test',
  component: MyCard,
  argTypes: {
    onPress: { action: 'pressed the button' },
    orientation: { control: 'radio', options: ['horizontal', 'vertical'] }
  },
  args: {
    title: "product 1",
    price: "500",
    discount: "30",
    percentage: 30,
    imageUrl: "https://loremflickr.com/cache/resized/65535_52587213378_618bddb4bc_b_640_480_nofilter.jpg",
    rating:5,
    orientation: 'vertical' , 
  },
  decorators: [
    (Story) => (
      <View style={{ backgroundColor :'white' ,  alignItems: 'center', justifyContent: 'center', flex: 1 }}>
        <Story />
      </View>
    ),
  ],
};

export default MyCardMeta;

export const VerticalCard = {};

export const HorizontalCard = {
  args: {
    title: "horizontal",
    price: "500",
    discount: "30",
    percentage: 30,
    imageUrl: "https://loremflickr.com/cache/resized/65535_52587213378_618bddb4bc_b_640_480_nofilter.jpg",
    rating:5,
    orientation: 'horizontal' , 
  },
};
