import { Api } from "./myApi";


export const service = new Api({
    baseUrl : "https://iit.aminaabdelkafi93.workers.dev",
    baseApiParams:{
        secure: true,
    },
    securityWorker  : (data)=>data

})
/*
headers:{
            authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NDNkMThlYS1hMzBlLTQ1NzctYjkxYi04YjkxMmRiNTBiMTQiLCJleHAiOjE3MDM3NTIwOTMsImlhdCI6MTcwMjQ1NjA5MywidHlwZSI6ImFjY2VzcyIsInJvbGVzIjpbInVzZXIiXX0.tlnAvyOXHnkIuNmZktYOsbPDonWivnD79B4mBJBQeAc"
        }
*/