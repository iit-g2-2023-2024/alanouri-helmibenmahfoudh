import React from "react";
import { View, Text, ImageBackground,StyleSheet, TouchableOpacity} from "react-native"; // Import necessary components from 'react-native'
import { Svg, Circle, Line, Path } from 'react-native-svg';
import Cardbody from "./CardBody";
import CardbodyHome from "./CardBodyHome";
import { router } from "expo-router";

const CardProductDetail = ({ element }) => {
  return (
    <TouchableOpacity key={element.id} style={styles.____________card} onPress={()=>{
        router.push({pathname:"(nothing)/[product]",
      params:{
        product: element.id
      }})
      }}
>
      <>
        <ImageBackground
          style={styles._image}
          source={{
            uri: element.image,
          }}
        />
        <CardbodyHome element={element} />
        {/* Vigma RN:: can be replaced with <________buttonfloating type={"filledCircle"} color={"light"} size={"small"} state={"default"} /> */}
        <View style={styles.________buttonfloating}>
          {/* Vigma RN:: can be replaced with <_________icon /> */}
          <View style={styles._________icon}>
            <View style={styles.____________boundingbox} />
            <Svg
                        style={styles.______vector}
                        width="14"
                        height="13"
                        viewBox="0 0 14 13"
                        fill="none"
                      >
                        <Path
                          d="M6.09998 11.8667L4.94998 10.8167C3.7722 9.73887 2.70831 8.66943 1.75831 7.60832C0.808313 6.54721 0.333313 5.37776 0.333313 4.09999C0.333313 3.05554 0.683313 2.18332 1.38331 1.48332C2.08331 0.783319 2.95554 0.433319 3.99998 0.433319C4.58887 0.433319 5.14442 0.558319 5.66665 0.808319C6.18887 1.05832 6.63331 1.39999 6.99998 1.83332C7.36665 1.39999 7.81109 1.05832 8.33331 0.808319C8.85554 0.558319 9.41109 0.433319 9.99998 0.433319C11.0444 0.433319 11.9166 0.783319 12.6166 1.48332C13.3166 2.18332 13.6666 3.05554 13.6666 4.09999C13.6666 5.37776 13.1944 6.54999 12.25 7.61665C11.3055 8.68332 10.2333 9.75554 9.03331 10.8333L7.89998 11.8667C7.64442 12.1111 7.34442 12.2333 6.99998 12.2333C6.65554 12.2333 6.35554 12.1111 6.09998 11.8667ZM6.36665 3.16665C6.04442 2.7111 5.69998 2.36387 5.33331 2.12499C4.96665 1.8861 4.5222 1.76665 3.99998 1.76665C3.33331 1.76665 2.77776 1.98887 2.33331 2.43332C1.88887 2.87776 1.66665 3.43332 1.66665 4.09999C1.66665 4.67776 1.8722 5.29165 2.28331 5.94165C2.69442 6.59165 3.18609 7.22221 3.75831 7.83332C4.33054 8.44443 4.91942 9.01665 5.52498 9.54998C6.13054 10.0833 6.6222 10.5222 6.99998 10.8667C7.37776 10.5222 7.86942 10.0833 8.47498 9.54998C9.08054 9.01665 9.66943 8.44443 10.2416 7.83332C10.8139 7.22221 11.3055 6.59165 11.7166 5.94165C12.1278 5.29165 12.3333 4.67776 12.3333 4.09999C12.3333 3.43332 12.1111 2.87776 11.6666 2.43332C11.2222 1.98887 10.6666 1.76665 9.99998 1.76665C9.47776 1.76665 9.03331 1.8861 8.66665 2.12499C8.29998 2.36387 7.95554 2.7111 7.63331 3.16665C7.55554 3.27776 7.46109 3.3611 7.34998 3.41665C7.23887 3.47221 7.1222 3.49999 6.99998 3.49999C6.87776 3.49999 6.76109 3.47221 6.64998 3.41665C6.53887 3.3611 6.44442 3.27776 6.36665 3.16665Z"
                          fill="#09111F"
                        />
                      </Svg>
          </View>
        </View>
      </>
    </TouchableOpacity>
  );
};

export default CardProductDetail;

const styles = StyleSheet.create({
    ____________card: {
        alignSelf: "stretch",
        flexShrink: 0,
        alignItems: "flex-start",
        rowGap: 0,
        borderRadius: 12,
        width: "47%",
        margin: 10, // Set width to 50%
      },

      _image: {
        alignSelf: "stretch",
        flexShrink: 0,
        height: 144,
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 0,
        borderLeftWidth: 1,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        borderColor: "rgba(225, 229, 235, 1)",
      },
      cardbody: {
        alignSelf: "stretch",
        flexShrink: 0,
        borderTopWidth: 0,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 12,
        borderBottomLeftRadius: 12,
        backgroundColor: "rgba(255, 255, 255, 1)",
        alignItems: "flex-start",
        rowGap: 2,
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderColor: "rgba(225, 229, 235, 1)",
      },
      _______________title: {
        alignSelf: "stretch",
        flexShrink: 0,
        height: 20,
        textAlign: "left",
        color: "rgba(9, 17, 31, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 14,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 20,
      },
      price: {
        alignSelf: "stretch",
        flexShrink: 0,
        paddingTop: 0,
        paddingBottom: 4,
        alignItems: "flex-start",
        rowGap: 0,
        paddingHorizontal: 0,
      },
      currentprice: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(9, 17, 31, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 16,
      },
      initialpricewrapper: {
        flexShrink: 0,
        flexDirection: "row",
        alignItems: "center",
        columnGap: 4,
      },
      initialprice: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(76, 89, 112, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "400",
        letterSpacing: 0,
        lineHeight: 16,
        textDecorationLine: "line-through",
      },
      _badge: {
        flexShrink: 0,
        backgroundColor: "rgba(215, 246, 228, 1)",
        flexDirection: "row",
        alignItems: "flex-start",
        columnGap: 0,
        paddingVertical: 0,
        paddingHorizontal: 4,
        borderRadius: 4,
      },
      label: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(8, 199, 84, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 16,
      },
      ratingProduct: {
        flexShrink: 0,
        paddingTop: 0,
        paddingBottom: 4,
        flexDirection: "row",
        alignItems: "flex-start",
        columnGap: 2,
        paddingHorizontal: 0,
      },
      iconsFilledStar: {
        flexShrink: 0,
        height: 16,
        width: 16,
        alignItems: "flex-start",
        rowGap: 0,
      },
      ___________boundingbox: {
        position: "absolute",
        flexShrink: 0,
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
      },
      _____vector: {
        position: "absolute",
        flexShrink: 0,
        top: 2,
        right: 2,
        bottom: 2,
        left: 2,
        overflow: "visible",
      },
      _label: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(76, 89, 112, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 16,
      },
      ________buttonfloating: {
        position: "absolute",
        flexShrink: 0,
        top: 12,
        right: 12,
        backgroundColor: "rgba(255, 255, 255, 1)",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        columnGap: 2,
        padding: 6,
        borderRadius: 9999,
      },
      _________icon: {
        flexShrink: 0,
        height: 16,
        width: 16,
        alignItems: "flex-start",
        rowGap: 0,
      },
      ____________boundingbox: {
        position: "absolute",
        flexShrink: 0,
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        
      },
      ______vector: {
        position: "absolute",
        flexShrink: 0,
        top: 2,
        right: 1,
        bottom: 2,
        left: 1,
        overflow: "visible",
      },
});


