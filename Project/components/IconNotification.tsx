import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { FontAwesome5, MaterialIcons } from '@expo/vector-icons'

interface IconNotificationProps {
  type: string
}

const IconNotification = (props: IconNotificationProps) => {
  return (
    <View>
        {
            props.type === 'Order' && <MaterialIcons name="receipt-long" size={24} color="black" />

        }{
            props.type === 'Promotion' && <FontAwesome5 name="sellcast" size={24} color="black" />
        }
    </View>
  )
}

export default IconNotification

const styles = StyleSheet.create({})
