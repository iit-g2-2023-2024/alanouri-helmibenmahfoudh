// LoginButton.js
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ImageSourcePropType } from 'react-native';

interface LoginButtonProps {
  imageSource: ImageSourcePropType;
  buttonText: string;
  isWideButton?: boolean;
  onPress?: () => void;
}

const LoginButton: React.FC<LoginButtonProps> = ({ imageSource, buttonText, isWideButton, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.buttonContainer, isWideButton && styles.wideButton]}>
        <Image source={imageSource} style={styles.icon} />
        <Text style={styles.buttonText}>{buttonText}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default LoginButton;

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:"center",
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 8,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#E1E5EB',
  },
  wideButton: {
    width: 312,
    marginRight: 16, 
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  buttonText: {
    color: 'black',
    fontWeight: 'bold',
    textAlignVertical: 'center', 
  },
});
