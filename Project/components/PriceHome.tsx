import React from 'react';
import { View, Text, StyleSheet } from 'react-native';



   const PriceHome =  ({  element }) => {

    return (
        <View style={styles.price}>
        <Text style={styles.currentprice}>{element.promotion}</Text>
        <View style={styles.initialpricewrapper}>
          <Text style={styles.initialprice}>{element.price}</Text>
          {/* Vigma RN:: can be replaced with <_badge type={"pill"} size={"small"} /> */}
          <View style={styles._badge}>
            <Text style={styles.label}>{`${Math.round(
              ((element.price - element.promotion) / element.price) * 100
            )}% `}</Text>
          </View>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
    price: {
        alignSelf: "stretch",
        flexShrink: 0,
        paddingTop: 0,
        paddingBottom: 4,
        alignItems: "flex-start",
        rowGap: 0,
        paddingHorizontal: 0,
      },
      currentprice: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(9, 17, 31, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 16,
      },
      initialpricewrapper: {
        flexShrink: 0,
        flexDirection: "row",
        alignItems: "center",
        columnGap: 4,
      },
      initialprice: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(76, 89, 112, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "400",
        letterSpacing: 0,
        lineHeight: 16,
        textDecorationLine: "line-through",
      },
      _badge: {
        flexShrink: 0,
        backgroundColor: "rgba(215, 246, 228, 1)",
        flexDirection: "row",
        alignItems: "flex-start",
        columnGap: 0,
        paddingVertical: 0,
        paddingHorizontal: 4,
        borderRadius: 4,
      },
      label: {
        flexShrink: 0,
        textAlign: "left",
        color: "rgba(8, 199, 84, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 12,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 16,
      },
})
export default PriceHome;