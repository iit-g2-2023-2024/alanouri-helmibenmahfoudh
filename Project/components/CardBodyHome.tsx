import React from 'react';
import { View, Text, StyleSheet,Image } from 'react-native';
import Price from './Price';
import Rate from './Rate';
import globalStyles from './globalStyles';
import PriceHome from './PriceHome';
import RateHome from './RatingHome';


 const CardbodyHome = ({ element}) => {
    return (
    		<View style={styles.cardbody}>
      			<Text style={styles._______________title}>{element.title}</Text>
      			<PriceHome element={element} 
                />
      			<RateHome   />
    		</View>
    )
}

const styles = StyleSheet.create({
    cardbody: {
        alignSelf: "stretch",
        flexShrink: 0,
        borderTopWidth: 0,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        borderBottomRightRadius: 12,
        borderBottomLeftRadius: 12,
        backgroundColor: "rgba(255, 255, 255, 1)",
        alignItems: "flex-start",
        rowGap: 2,
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderColor: "rgba(225, 229, 235, 1)",
      },
      _______________title: {
        alignSelf: "stretch",
        flexShrink: 0,
        height: 20,
        textAlign: "left",
        color: "rgba(9, 17, 31, 1)",
        fontFamily: "Satoshi Variable",
        fontSize: 14,
        fontWeight: "700",
        letterSpacing: 0,
        lineHeight: 20,
      },
})
export default CardbodyHome;