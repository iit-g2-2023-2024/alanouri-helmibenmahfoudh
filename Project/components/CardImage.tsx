import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import globalStyles from './globalStyles'; 
import { Path, Svg } from 'react-native-svg';

const getRandomImage = () => {
  const width = 116; // Set your desired width
  const height = 154; // Set your desired height
  const accessKey = 'lUmNmuKO4wv_ex0r6QGcieKO7GB5qmhNiW4G-b0tVGs'; // Replace with your Unsplash access key
  const randomImageId = Math.floor(Math.random() * 1000) + 1; // Generate a random image ID

  return `https://source.unsplash.com/${width}x${height}/?sig=${randomImageId}&client_id=${accessKey}`;
};

const CardImage = ({ element }) => {


  return (
    <View key={element.id} >
            {/* Vigma RN:: can be replaced with <____card type={"fullImage"} orientation={"vertical"} size={"small"} /> */}
            <ImageBackground 
              style={styles.____card}
              source={{
                uri: /* dummy image */ getRandomImage(),
              }}
            >
              <View style={styles._______text}>
                <Text style={styles.______title}>{element.name}</Text>
              </View>
              {/* Vigma RN:: can be replaced with <Buttonfloating type={"filledCircle"} color={"light"} size={"small"} state={"default"} /> */}
              <View style={styles.buttonfloating}>
                {/* Vigma RN:: can be replaced with <_icon /> */}
                <View style={styles._icon}>
                  <View style={styles.___boundingbox} />
                  <Svg
                    style={styles.___vector}
                    width="12"
                    height="12"
                    viewBox="0 0 12 12"
                    fill="none"
                  >
                    <Path
                      d="M5.53335 10.8667C5.41113 10.7444 5.34724 10.5889 5.34169 10.4C5.33613 10.2111 5.39446 10.0556 5.51669 9.93333L8.78335 6.66666H1.33335C1.14446 6.66666 0.986131 6.60278 0.858354 6.475C0.730576 6.34722 0.666687 6.18889 0.666687 6C0.666687 5.81111 0.730576 5.65277 0.858354 5.525C0.986131 5.39722 1.14446 5.33333 1.33335 5.33333H8.78335L5.51669 2.06666C5.39446 1.94444 5.33613 1.78889 5.34169 1.6C5.34724 1.41111 5.41113 1.25555 5.53335 1.13333C5.65558 1.01111 5.81113 0.949997 6.00002 0.949997C6.18891 0.949997 6.34446 1.01111 6.46669 1.13333L10.8667 5.53333C10.9334 5.58889 10.9806 5.65833 11.0084 5.74166C11.0361 5.825 11.05 5.91111 11.05 6C11.05 6.08889 11.0361 6.17222 11.0084 6.25C10.9806 6.32778 10.9334 6.4 10.8667 6.46666L6.46669 10.8667C6.34446 10.9889 6.18891 11.05 6.00002 11.05C5.81113 11.05 5.65558 10.9889 5.53335 10.8667Z"
                      fill="#09111F"
                    />
                  </Svg>
                </View>
              </View>
            </ImageBackground>
            </View>
  
  );
};

const styles = StyleSheet.create({
  ____card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _______text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ______title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  ___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible",
  },
});

export default CardImage;
