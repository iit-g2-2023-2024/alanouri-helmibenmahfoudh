import { StyleSheet } from 'react-native';

const globalStyles = StyleSheet.create({
    title: {
        alignSelf: "stretch",
        flexShrink: 0,
        textAlign: "left",
        fontFamily: "Satoshi Variable",
        fontWeight: "700",
        letterSpacing: 0,
      },
    
});
export default globalStyles;