import React from 'react';
import { View, Text, ImageBackground, StyleSheet, Dimensions } from 'react-native';
import { Svg, Path, Line } from 'react-native-svg';

export default function OrderDetails() {
  const screenWidth = Dimensions.get("window").width;

    return (
    		<View style={styles.orderDetails}>
      			<View style={styles.orderstatus}>
        				<View style={styles.message}  >
          					{/* Vigma RN:: can be replaced with <IconsOutlineOrder_approve  /> */}
          					<View style={styles.iconsOutlineOrder_approve}>
            						<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="27" height="27" viewBox="0 0 27 27" fill="none" >
<Path d="M19.0333 20.1667L18.4667 19.6C18.2667 19.4 18.0278 19.3 17.75 19.3C17.4722 19.3 17.2333 19.4 17.0333 19.6C16.8333 19.8 16.7333 20.0333 16.7333 20.3C16.7333 20.5667 16.8333 20.8 17.0333 21L18.3333 22.3C18.5333 22.5 18.7667 22.6 19.0333 22.6C19.3 22.6 19.5333 22.5 19.7333 22.3L22.9667 19.1333C23.1667 18.9333 23.2667 18.6944 23.2667 18.4167C23.2667 18.1389 23.1667 17.9 22.9667 17.7C22.7667 17.5 22.5278 17.4 22.25 17.4C21.9722 17.4 21.7333 17.5 21.5333 17.7L19.0333 20.1667ZM5.33333 8H18.6667C19.0444 8 19.3611 7.87222 19.6167 7.61667C19.8722 7.36111 20 7.04444 20 6.66667C20 6.28889 19.8722 5.97222 19.6167 5.71667C19.3611 5.46111 19.0444 5.33333 18.6667 5.33333H5.33333C4.95556 5.33333 4.63889 5.46111 4.38333 5.71667C4.12778 5.97222 4 6.28889 4 6.66667C4 7.04444 4.12778 7.36111 4.38333 7.61667C4.63889 7.87222 4.95556 8 5.33333 8ZM20 26.6667C18.1556 26.6667 16.5833 26.0167 15.2833 24.7167C13.9833 23.4167 13.3333 21.8444 13.3333 20C13.3333 18.1556 13.9833 16.5833 15.2833 15.2833C16.5833 13.9833 18.1556 13.3333 20 13.3333C21.8444 13.3333 23.4167 13.9833 24.7167 15.2833C26.0167 16.5833 26.6667 18.1556 26.6667 20C26.6667 21.8444 26.0167 23.4167 24.7167 24.7167C23.4167 26.0167 21.8444 26.6667 20 26.6667ZM0 25.1667V2.66667C0 1.93333 0.261111 1.30556 0.783333 0.783333C1.30556 0.261111 1.93333 0 2.66667 0H21.3333C22.0667 0 22.6944 0.261111 23.2167 0.783333C23.7389 1.30556 24 1.93333 24 2.66667V11.5667C23.5778 11.3667 23.1444 11.2 22.7 11.0667C22.2556 10.9333 21.8 10.8333 21.3333 10.7667V2.66667H2.66667V21.4H10.7667C10.8778 22.0889 11.05 22.7444 11.2833 23.3667C11.5167 23.9889 11.8222 24.5778 12.2 25.1333C12.0889 25.1556 11.9722 25.1389 11.85 25.0833C11.7278 25.0278 11.6222 24.9556 11.5333 24.8667L10.4667 23.8C10.3333 23.6667 10.1778 23.6 10 23.6C9.82222 23.6 9.66667 23.6667 9.53333 23.8L8.46667 24.8667C8.33333 25 8.17778 25.0667 8 25.0667C7.82222 25.0667 7.66667 25 7.53333 24.8667L6.46667 23.8C6.33333 23.6667 6.17778 23.6 6 23.6C5.82222 23.6 5.66667 23.6667 5.53333 23.8L4.46667 24.8667C4.33333 25 4.17778 25.0667 4 25.0667C3.82222 25.0667 3.66667 25 3.53333 24.8667L2.46667 23.8C2.33333 23.6667 2.17778 23.6 2 23.6C1.82222 23.6 1.66667 23.6667 1.53333 23.8L0.466667 24.8667L0 25.1667ZM5.33333 18.6667H10.7667C10.8333 18.2 10.9333 17.7444 11.0667 17.3C11.2 16.8556 11.3667 16.4222 11.5667 16H5.33333C4.95556 16 4.63889 16.1278 4.38333 16.3833C4.12778 16.6389 4 16.9556 4 17.3333C4 17.7111 4.12778 18.0278 4.38333 18.2833C4.63889 18.5389 4.95556 18.6667 5.33333 18.6667ZM5.33333 13.3333H13.4667C14.3111 12.5111 15.2944 11.8611 16.4167 11.3833C17.5389 10.9056 18.7333 10.6667 20 10.6667H5.33333C4.95556 10.6667 4.63889 10.7944 4.38333 11.05C4.12778 11.3056 4 11.6222 4 12C4 12.3778 4.12778 12.6944 4.38333 12.95C4.63889 13.2056 4.95556 13.3333 5.33333 13.3333Z" fill="white"/>
</Svg>

          					</View>
          					<View style={styles.text}>
            						<Text style={styles.title}>
              							{`Order completed`}
            						</Text>
            						<Text style={styles.subtitle}>
              							{`Thank you for shopping with us!`}
            						</Text>
          					</View>
        				</View>
        				<View style={styles.steps}>
          					<View style={styles.payed}>
            						{/* Vigma RN:: can be replaced with <IconsOutlineAccount_balance_wallet  /> */}
            						<View style={styles.iconsOutlineAccount_balance_wallet}>
              							<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="19" height="18" viewBox="0 0 19 18" fill="none" >
<Path d="M13 10.5C13.4333 10.5 13.7917 10.3583 14.075 10.075C14.3583 9.79167 14.5 9.43333 14.5 9C14.5 8.56667 14.3583 8.20833 14.075 7.925C13.7917 7.64167 13.4333 7.5 13 7.5C12.5667 7.5 12.2083 7.64167 11.925 7.925C11.6417 8.20833 11.5 8.56667 11.5 9C11.5 9.43333 11.6417 9.79167 11.925 10.075C12.2083 10.3583 12.5667 10.5 13 10.5ZM2 18C1.45 18 0.979167 17.8042 0.5875 17.4125C0.195833 17.0208 0 16.55 0 16V2C0 1.45 0.195833 0.979167 0.5875 0.5875C0.979167 0.195833 1.45 0 2 0H16C16.55 0 17.0208 0.195833 17.4125 0.5875C17.8042 0.979167 18 1.45 18 2V4.5H16V2H2V16H16V13.5H18V16C18 16.55 17.8042 17.0208 17.4125 17.4125C17.0208 17.8042 16.55 18 16 18H2ZM10 14C9.45 14 8.97917 13.8042 8.5875 13.4125C8.19583 13.0208 8 12.55 8 12V6C8 5.45 8.19583 4.97917 8.5875 4.5875C8.97917 4.19583 9.45 4 10 4H17C17.55 4 18.0208 4.19583 18.4125 4.5875C18.8042 4.97917 19 5.45 19 6V12C19 12.55 18.8042 13.0208 18.4125 13.4125C18.0208 13.8042 17.55 14 17 14H10Z" fill="#09111F"/>
</Svg>

            						</View>
            						{/* Vigma RN:: can be replaced with <IconsFilledCheck_circle  /> */}
            						<View style={styles.iconsFilledCheck_circle}>
              							<View style={styles.__boundingbox}/>
<Svg style={styles.__vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06667 8.20016L4.63334 6.76683C4.51111 6.64461 4.35556 6.5835 4.16667 6.5835C3.97778 6.5835 3.82223 6.64461 3.7 6.76683C3.57778 6.88905 3.51667 7.04461 3.51667 7.2335C3.51667 7.42239 3.57778 7.57794 3.7 7.70016L5.6 9.60016C5.73334 9.7335 5.88889 9.80016 6.06667 9.80016C6.24445 9.80016 6.4 9.7335 6.53334 9.60016L10.3 5.8335C10.4222 5.71127 10.4833 5.55572 10.4833 5.36683C10.4833 5.17794 10.4222 5.02239 10.3 4.90016C10.1778 4.77794 10.0222 4.71683 9.83334 4.71683C9.64445 4.71683 9.48889 4.77794 9.36667 4.90016L6.06667 8.20016ZM7 13.6668C6.07778 13.6668 5.21111 13.4918 4.4 13.1418C3.58889 12.7918 2.88334 12.3168 2.28334 11.7168C1.68334 11.1168 1.20834 10.4113 0.858336 9.60016C0.508336 8.78905 0.333336 7.92239 0.333336 7.00016C0.333336 6.07794 0.508336 5.21127 0.858336 4.40016C1.20834 3.58905 1.68334 2.8835 2.28334 2.2835C2.88334 1.6835 3.58889 1.2085 4.4 0.858496C5.21111 0.508496 6.07778 0.333496 7 0.333496C7.92222 0.333496 8.78889 0.508496 9.6 0.858496C10.4111 1.2085 11.1167 1.6835 11.7167 2.2835C12.3167 2.8835 12.7917 3.58905 13.1417 4.40016C13.4917 5.21127 13.6667 6.07794 13.6667 7.00016C13.6667 7.92239 13.4917 8.78905 13.1417 9.60016C12.7917 10.4113 12.3167 11.1168 11.7167 11.7168C11.1167 12.3168 10.4111 12.7918 9.6 13.1418C8.78889 13.4918 7.92222 13.6668 7 13.6668Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <Divider type={"horizontal"} /> */}
          					<View style={styles.divider}>
            						<View style={styles.linewrapper}>
<Svg style={styles.line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
            						<View style={styles._linewrapper}>
<Svg style={styles._line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
          					</View>
          					<View style={styles.packed}>
            						{/* Vigma RN:: can be replaced with <IconsOutlinePackage  /> */}
            						<View style={styles.iconsOutlinePackage}>
              							<View style={styles.___boundingbox}/>
<Svg style={styles.___vector} width="18" height="18" viewBox="0 0 18 18" fill="none" >
<Path d="M7 6.75L9 5.75L11 6.75V2H7V6.75ZM4 14V12H9V14H4ZM2 18C1.45 18 0.979167 17.8042 0.5875 17.4125C0.195833 17.0208 0 16.55 0 16V2C0 1.45 0.195833 0.979167 0.5875 0.5875C0.979167 0.195833 1.45 0 2 0H16C16.55 0 17.0208 0.195833 17.4125 0.5875C17.8042 0.979167 18 1.45 18 2V16C18 16.55 17.8042 17.0208 17.4125 17.4125C17.0208 17.8042 16.55 18 16 18H2ZM2 16H16V2H13V10L9 8L5 10V2H2V16Z" fill="#09111F"/>
</Svg>

            						</View>
            						{/* Vigma RN:: can be replaced with <_iconsFilledCheck_circle  /> */}
            						<View style={styles._iconsFilledCheck_circle}>
              							<View style={styles.____boundingbox}/>
<Svg style={styles.____vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06666 8.20016L4.63333 6.76683C4.51111 6.64461 4.35555 6.5835 4.16666 6.5835C3.97777 6.5835 3.82222 6.64461 3.7 6.76683C3.57777 6.88905 3.51666 7.04461 3.51666 7.2335C3.51666 7.42239 3.57777 7.57794 3.7 7.70016L5.6 9.60016C5.73333 9.7335 5.88888 9.80016 6.06666 9.80016C6.24444 9.80016 6.4 9.7335 6.53333 9.60016L10.3 5.8335C10.4222 5.71127 10.4833 5.55572 10.4833 5.36683C10.4833 5.17794 10.4222 5.02239 10.3 4.90016C10.1778 4.77794 10.0222 4.71683 9.83333 4.71683C9.64444 4.71683 9.48888 4.77794 9.36666 4.90016L6.06666 8.20016ZM7 13.6668C6.07777 13.6668 5.21111 13.4918 4.39999 13.1418C3.58888 12.7918 2.88333 12.3168 2.28333 11.7168C1.68333 11.1168 1.20833 10.4113 0.858328 9.60016C0.508328 8.78905 0.333328 7.92239 0.333328 7.00016C0.333328 6.07794 0.508328 5.21127 0.858328 4.40016C1.20833 3.58905 1.68333 2.8835 2.28333 2.2835C2.88333 1.6835 3.58888 1.2085 4.39999 0.858496C5.21111 0.508496 6.07777 0.333496 7 0.333496C7.92222 0.333496 8.78888 0.508496 9.6 0.858496C10.4111 1.2085 11.1167 1.6835 11.7167 2.2835C12.3167 2.8835 12.7917 3.58905 13.1417 4.40016C13.4917 5.21127 13.6667 6.07794 13.6667 7.00016C13.6667 7.92239 13.4917 8.78905 13.1417 9.60016C12.7917 10.4113 12.3167 11.1168 11.7167 11.7168C11.1167 12.3168 10.4111 12.7918 9.6 13.1418C8.78888 13.4918 7.92222 13.6668 7 13.6668Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <_divider type={"horizontal"} /> */}
          					<View style={styles._divider}>
            						<View style={styles.__linewrapper}>
<Svg style={styles.__line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
            						<View style={styles.___linewrapper}>
<Svg style={styles.___line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
          					</View>
          					<View style={styles.deliver}>
            						{/* Vigma RN:: can be replaced with <IconsOutlineLocal_shipping  /> */}
            						<View style={styles.iconsOutlineLocal_shipping}>
              							<View style={styles._____boundingbox}/>
<Svg style={styles._____vector} width="22" height="16" viewBox="0 0 22 16" fill="none" >
<Path d="M5 16C4.16667 16 3.45833 15.7083 2.875 15.125C2.29167 14.5417 2 13.8333 2 13C1.45 13 0.979167 12.8042 0.5875 12.4125C0.195833 12.0208 0 11.55 0 11V2C0 1.45 0.195833 0.979167 0.5875 0.5875C0.979167 0.195833 1.45 0 2 0H14C14.55 0 15.0208 0.195833 15.4125 0.5875C15.8042 0.979167 16 1.45 16 2V4H18.5C18.6667 4 18.8167 4.03333 18.95 4.1C19.0833 4.16667 19.2 4.26667 19.3 4.4L21.8 7.725C21.8667 7.80833 21.9167 7.9 21.95 8C21.9833 8.1 22 8.20833 22 8.325V12C22 12.2833 21.9042 12.5208 21.7125 12.7125C21.5208 12.9042 21.2833 13 21 13H20C20 13.8333 19.7083 14.5417 19.125 15.125C18.5417 15.7083 17.8333 16 17 16C16.1667 16 15.4583 15.7083 14.875 15.125C14.2917 14.5417 14 13.8333 14 13H8C8 13.8333 7.70833 14.5417 7.125 15.125C6.54167 15.7083 5.83333 16 5 16ZM5 14C5.28333 14 5.52083 13.9042 5.7125 13.7125C5.90417 13.5208 6 13.2833 6 13C6 12.7167 5.90417 12.4792 5.7125 12.2875C5.52083 12.0958 5.28333 12 5 12C4.71667 12 4.47917 12.0958 4.2875 12.2875C4.09583 12.4792 4 12.7167 4 13C4 13.2833 4.09583 13.5208 4.2875 13.7125C4.47917 13.9042 4.71667 14 5 14ZM2 2V11H2.8C3.08333 10.7 3.40833 10.4583 3.775 10.275C4.14167 10.0917 4.55 10 5 10C5.45 10 5.85833 10.0917 6.225 10.275C6.59167 10.4583 6.91667 10.7 7.2 11H14V2H2ZM17 14C17.2833 14 17.5208 13.9042 17.7125 13.7125C17.9042 13.5208 18 13.2833 18 13C18 12.7167 17.9042 12.4792 17.7125 12.2875C17.5208 12.0958 17.2833 12 17 12C16.7167 12 16.4792 12.0958 16.2875 12.2875C16.0958 12.4792 16 12.7167 16 13C16 13.2833 16.0958 13.5208 16.2875 13.7125C16.4792 13.9042 16.7167 14 17 14ZM16 9H20.25L18 6H16V9Z" fill="#09111F"/>
</Svg>

            						</View>
            						{/* Vigma RN:: can be replaced with <__iconsFilledCheck_circle  /> */}
            						<View style={styles.__iconsFilledCheck_circle}>
              							<View style={styles.______boundingbox}/>
<Svg style={styles.______vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06668 8.20016L4.63334 6.76683C4.51112 6.64461 4.35557 6.5835 4.16668 6.5835C3.97779 6.5835 3.82223 6.64461 3.70001 6.76683C3.57779 6.88905 3.51668 7.04461 3.51668 7.2335C3.51668 7.42239 3.57779 7.57794 3.70001 7.70016L5.60001 9.60016C5.73334 9.7335 5.8889 9.80016 6.06668 9.80016C6.24445 9.80016 6.40001 9.7335 6.53334 9.60016L10.3 5.8335C10.4222 5.71127 10.4833 5.55572 10.4833 5.36683C10.4833 5.17794 10.4222 5.02239 10.3 4.90016C10.1778 4.77794 10.0222 4.71683 9.83334 4.71683C9.64445 4.71683 9.4889 4.77794 9.36668 4.90016L6.06668 8.20016ZM7.00001 13.6668C6.07779 13.6668 5.21112 13.4918 4.40001 13.1418C3.5889 12.7918 2.88334 12.3168 2.28334 11.7168C1.68334 11.1168 1.20834 10.4113 0.858344 9.60016C0.508344 8.78905 0.333344 7.92239 0.333344 7.00016C0.333344 6.07794 0.508344 5.21127 0.858344 4.40016C1.20834 3.58905 1.68334 2.8835 2.28334 2.2835C2.88334 1.6835 3.5889 1.2085 4.40001 0.858496C5.21112 0.508496 6.07779 0.333496 7.00001 0.333496C7.92223 0.333496 8.7889 0.508496 9.60001 0.858496C10.4111 1.2085 11.1167 1.6835 11.7167 2.2835C12.3167 2.8835 12.7917 3.58905 13.1417 4.40016C13.4917 5.21127 13.6667 6.07794 13.6667 7.00016C13.6667 7.92239 13.4917 8.78905 13.1417 9.60016C12.7917 10.4113 12.3167 11.1168 11.7167 11.7168C11.1167 12.3168 10.4111 12.7918 9.60001 13.1418C8.7889 13.4918 7.92223 13.6668 7.00001 13.6668Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <__divider type={"horizontal"} /> */}
          					<View style={styles.__divider}>
            						<View style={styles.____linewrapper}>
<Svg style={styles.____line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
            						<View style={styles._____linewrapper}>
<Svg style={styles._____line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
          					</View>
          					<View style={styles.delivered}>
            						{/* Vigma RN:: can be replaced with <IconsOutlineDoorbell_3p  /> */}
            						<View style={styles.iconsOutlineDoorbell_3p}>
              							<View style={styles._______boundingbox}/>
<Svg style={styles._______vector} width="14" height="20" viewBox="0 0 14 20" fill="none" >
<Path d="M2 20C1.45 20 0.979167 19.8042 0.5875 19.4125C0.195833 19.0208 0 18.55 0 18V2C0 1.45 0.195833 0.979167 0.5875 0.5875C0.979167 0.195833 1.45 0 2 0H12C12.55 0 13.0208 0.195833 13.4125 0.5875C13.8042 0.979167 14 1.45 14 2V18C14 18.55 13.8042 19.0208 13.4125 19.4125C13.0208 19.8042 12.55 20 12 20H2ZM2 18H12V2H2V18ZM7 17C7.55 17 8.02083 16.8042 8.4125 16.4125C8.80417 16.0208 9 15.55 9 15C9 14.45 8.80417 13.9792 8.4125 13.5875C8.02083 13.1958 7.55 13 7 13C6.45 13 5.97917 13.1958 5.5875 13.5875C5.19583 13.9792 5 14.45 5 15C5 15.55 5.19583 16.0208 5.5875 16.4125C5.97917 16.8042 6.45 17 7 17ZM7 16C6.71667 16 6.47917 15.9042 6.2875 15.7125C6.09583 15.5208 6 15.2833 6 15C6 14.7167 6.09583 14.4792 6.2875 14.2875C6.47917 14.0958 6.71667 14 7 14C7.28333 14 7.52083 14.0958 7.7125 14.2875C7.90417 14.4792 8 14.7167 8 15C8 15.2833 7.90417 15.5208 7.7125 15.7125C7.52083 15.9042 7.28333 16 7 16ZM7 11.5C7.3 11.5 7.54167 11.4083 7.725 11.225C7.90833 11.0417 8 10.8 8 10.5H6C6 10.8 6.09167 11.0417 6.275 11.225C6.45833 11.4083 6.7 11.5 7 11.5ZM3.5 10H10.5C10.6333 10 10.75 9.95 10.85 9.85C10.95 9.75 11 9.63333 11 9.5C11 9.36667 10.95 9.25 10.85 9.15C10.75 9.05 10.6333 9 10.5 9H10V6.7C10 5.95 9.80833 5.27917 9.425 4.6875C9.04167 4.09583 8.5 3.7 7.8 3.5V3.2C7.8 2.96667 7.725 2.775 7.575 2.625C7.425 2.475 7.23333 2.4 7 2.4C6.76667 2.4 6.575 2.475 6.425 2.625C6.275 2.775 6.2 2.96667 6.2 3.2V3.5C5.5 3.75 4.95833 4.15833 4.575 4.725C4.19167 5.29167 4 5.95 4 6.7V9H3.5C3.36667 9 3.25 9.05 3.15 9.15C3.05 9.25 3 9.36667 3 9.5C3 9.63333 3.05 9.75 3.15 9.85C3.25 9.95 3.36667 10 3.5 10Z" fill="#09111F"/>
</Svg>

            						</View>
            						{/* Vigma RN:: can be replaced with <___iconsFilledCheck_circle  /> */}
            						<View style={styles.___iconsFilledCheck_circle}>
              							<View style={styles.________boundingbox}/>
<Svg style={styles.________vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06668 8.20016L4.63334 6.76683C4.51112 6.64461 4.35557 6.5835 4.16668 6.5835C3.97779 6.5835 3.82223 6.64461 3.70001 6.76683C3.57779 6.88905 3.51668 7.04461 3.51668 7.2335C3.51668 7.42239 3.57779 7.57794 3.70001 7.70016L5.60001 9.60016C5.73334 9.7335 5.8889 9.80016 6.06668 9.80016C6.24445 9.80016 6.40001 9.7335 6.53334 9.60016L10.3 5.8335C10.4222 5.71127 10.4833 5.55572 10.4833 5.36683C10.4833 5.17794 10.4222 5.02239 10.3 4.90016C10.1778 4.77794 10.0222 4.71683 9.83334 4.71683C9.64445 4.71683 9.4889 4.77794 9.36668 4.90016L6.06668 8.20016ZM7.00001 13.6668C6.07779 13.6668 5.21112 13.4918 4.40001 13.1418C3.5889 12.7918 2.88334 12.3168 2.28334 11.7168C1.68334 11.1168 1.20834 10.4113 0.858344 9.60016C0.508344 8.78905 0.333344 7.92239 0.333344 7.00016C0.333344 6.07794 0.508344 5.21127 0.858344 4.40016C1.20834 3.58905 1.68334 2.8835 2.28334 2.2835C2.88334 1.6835 3.5889 1.2085 4.40001 0.858496C5.21112 0.508496 6.07779 0.333496 7.00001 0.333496C7.92223 0.333496 8.7889 0.508496 9.60001 0.858496C10.4111 1.2085 11.1167 1.6835 11.7167 2.2835C12.3167 2.8835 12.7917 3.58905 13.1417 4.40016C13.4917 5.21127 13.6667 6.07794 13.6667 7.00016C13.6667 7.92239 13.4917 8.78905 13.1417 9.60016C12.7917 10.4113 12.3167 11.1168 11.7167 11.7168C11.1167 12.3168 10.4111 12.7918 9.60001 13.1418C8.7889 13.4918 7.92223 13.6668 7.00001 13.6668Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <___divider type={"horizontal"} /> */}
          					<View style={styles.___divider}>
            						<View style={styles.______linewrapper}>
<Svg style={styles.______line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
            						<View style={styles._______linewrapper}>
<Svg style={styles._______line} width="24" height="1" viewBox="0 0 24 1" fill="none" >
<Line y1="0.5" x2="24" y2="0.5" stroke="#09111F"/>
</Svg>

            						</View>
          					</View>
          					<View style={styles.completed}>
            						{/* Vigma RN:: can be replaced with <_iconsOutlineOrder_approve  /> */}
            						<View style={styles._iconsOutlineOrder_approve}>
              							<View style={styles._________boundingbox}/>
<Svg style={styles._________vector} width="20" height="20" viewBox="0 0 20 20" fill="none" >
<Path d="M14.275 15.125L13.85 14.7C13.7 14.55 13.5208 14.475 13.3125 14.475C13.1042 14.475 12.925 14.55 12.775 14.7C12.625 14.85 12.55 15.025 12.55 15.225C12.55 15.425 12.625 15.6 12.775 15.75L13.75 16.725C13.9 16.875 14.075 16.95 14.275 16.95C14.475 16.95 14.65 16.875 14.8 16.725L17.225 14.35C17.375 14.2 17.45 14.0208 17.45 13.8125C17.45 13.6042 17.375 13.425 17.225 13.275C17.075 13.125 16.8958 13.05 16.6875 13.05C16.4792 13.05 16.3 13.125 16.15 13.275L14.275 15.125ZM4 6H14C14.2833 6 14.5208 5.90417 14.7125 5.7125C14.9042 5.52083 15 5.28333 15 5C15 4.71667 14.9042 4.47917 14.7125 4.2875C14.5208 4.09583 14.2833 4 14 4H4C3.71667 4 3.47917 4.09583 3.2875 4.2875C3.09583 4.47917 3 4.71667 3 5C3 5.28333 3.09583 5.52083 3.2875 5.7125C3.47917 5.90417 3.71667 6 4 6ZM15 20C13.6167 20 12.4375 19.5125 11.4625 18.5375C10.4875 17.5625 10 16.3833 10 15C10 13.6167 10.4875 12.4375 11.4625 11.4625C12.4375 10.4875 13.6167 10 15 10C16.3833 10 17.5625 10.4875 18.5375 11.4625C19.5125 12.4375 20 13.6167 20 15C20 16.3833 19.5125 17.5625 18.5375 18.5375C17.5625 19.5125 16.3833 20 15 20ZM0 18.875V2C0 1.45 0.195833 0.979167 0.5875 0.5875C0.979167 0.195833 1.45 0 2 0H16C16.55 0 17.0208 0.195833 17.4125 0.5875C17.8042 0.979167 18 1.45 18 2V8.675C17.6833 8.525 17.3583 8.4 17.025 8.3C16.6917 8.2 16.35 8.125 16 8.075V2H2V16.05H8.075C8.15833 16.5667 8.2875 17.0583 8.4625 17.525C8.6375 17.9917 8.86667 18.4333 9.15 18.85C9.06667 18.8667 8.97917 18.8542 8.8875 18.8125C8.79583 18.7708 8.71667 18.7167 8.65 18.65L7.85 17.85C7.75 17.75 7.63333 17.7 7.5 17.7C7.36667 17.7 7.25 17.75 7.15 17.85L6.35 18.65C6.25 18.75 6.13333 18.8 6 18.8C5.86667 18.8 5.75 18.75 5.65 18.65L4.85 17.85C4.75 17.75 4.63333 17.7 4.5 17.7C4.36667 17.7 4.25 17.75 4.15 17.85L3.35 18.65C3.25 18.75 3.13333 18.8 3 18.8C2.86667 18.8 2.75 18.75 2.65 18.65L1.85 17.85C1.75 17.75 1.63333 17.7 1.5 17.7C1.36667 17.7 1.25 17.75 1.15 17.85L0.35 18.65L0 18.875ZM4 14H8.075C8.125 13.65 8.2 13.3083 8.3 12.975C8.4 12.6417 8.525 12.3167 8.675 12H4C3.71667 12 3.47917 12.0958 3.2875 12.2875C3.09583 12.4792 3 12.7167 3 13C3 13.2833 3.09583 13.5208 3.2875 13.7125C3.47917 13.9042 3.71667 14 4 14ZM4 10H10.1C10.7333 9.38333 11.4708 8.89583 12.3125 8.5375C13.1542 8.17917 14.05 8 15 8H4C3.71667 8 3.47917 8.09583 3.2875 8.2875C3.09583 8.47917 3 8.71667 3 9C3 9.28333 3.09583 9.52083 3.2875 9.7125C3.47917 9.90417 3.71667 10 4 10Z" fill="#09111F"/>
</Svg>

            						</View>
            						{/* Vigma RN:: can be replaced with <____iconsFilledCheck_circle  /> */}
            						<View style={styles.____iconsFilledCheck_circle}>
              							<View style={styles.__________boundingbox}/>
<Svg style={styles.__________vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.06668 8.20016L4.63334 6.76683C4.51112 6.64461 4.35557 6.5835 4.16668 6.5835C3.97779 6.5835 3.82223 6.64461 3.70001 6.76683C3.57779 6.88905 3.51668 7.04461 3.51668 7.2335C3.51668 7.42239 3.57779 7.57794 3.70001 7.70016L5.60001 9.60016C5.73334 9.7335 5.8889 9.80016 6.06668 9.80016C6.24445 9.80016 6.40001 9.7335 6.53334 9.60016L10.3 5.8335C10.4222 5.71127 10.4833 5.55572 10.4833 5.36683C10.4833 5.17794 10.4222 5.02239 10.3 4.90016C10.1778 4.77794 10.0222 4.71683 9.83334 4.71683C9.64445 4.71683 9.4889 4.77794 9.36668 4.90016L6.06668 8.20016ZM7.00001 13.6668C6.07779 13.6668 5.21112 13.4918 4.40001 13.1418C3.5889 12.7918 2.88334 12.3168 2.28334 11.7168C1.68334 11.1168 1.20834 10.4113 0.858344 9.60016C0.508344 8.78905 0.333344 7.92239 0.333344 7.00016C0.333344 6.07794 0.508344 5.21127 0.858344 4.40016C1.20834 3.58905 1.68334 2.8835 2.28334 2.2835C2.88334 1.6835 3.5889 1.2085 4.40001 0.858496C5.21112 0.508496 6.07779 0.333496 7.00001 0.333496C7.92223 0.333496 8.7889 0.508496 9.60001 0.858496C10.4111 1.2085 11.1167 1.6835 11.7167 2.2835C12.3167 2.8835 12.7917 3.58905 13.1417 4.40016C13.4917 5.21127 13.6667 6.07794 13.6667 7.00016C13.6667 7.92239 13.4917 8.78905 13.1417 9.60016C12.7917 10.4113 12.3167 11.1168 11.7167 11.7168C11.1167 12.3168 10.4111 12.7918 9.60001 13.1418C8.7889 13.4918 7.92223 13.6668 7.00001 13.6668Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
      			</View>
      			<View style={styles.shippinginformation}>
        				<View style={styles.header}>
          					<Text style={styles._title}>
            						{`Shipping information`}
          					</Text>
        				</View>
        				<View style={styles.items}>
          					<View style={styles.item}>
            						<Text style={styles.__title}>
              							{`Order has been received`}
            						</Text>
            						<Text style={styles._subtitle}>
              							{`20-12-2023 11:00`}
            						</Text>
          					</View>
          					<View style={styles._item}>
            						<Text style={styles.___title}>
              							{`The order is being delivered to the destination address`}
            						</Text>
            						<Text style={styles.__subtitle}>
              							{`20-12-2023 10:00`}
            						</Text>
          					</View>
          					<View style={styles.__item}>
            						<Text style={styles.____title}>
              							{`The order has arrived at the transit location`}
            						</Text>
            						<Text style={styles.___subtitle}>
              							{`20-12-2023 09:00`}
            						</Text>
          					</View>
          					<View style={styles.___item}>
            						<Text style={styles._____title}>
              							{`The order is on its way to the transit location`}
            						</Text>
            						<Text style={styles.____subtitle}>
              							{`20-12-2023 08:00`}
            						</Text>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <Buttonviewmore color={"light"} size={"small"} state={"default"} /> */}
        				<View style={styles.buttonviewmore}>
          					<View style={styles.textwrapper}>
            						<Text style={styles._text}>
              							{`View more`}
            						</Text>
          					</View>
          					{/* Vigma RN:: can be replaced with <Icon  /> */}
          					<View style={styles.icon}>
            						<View style={styles.___________boundingbox}/>
<Svg style={styles.___________vector} width="8" height="5" viewBox="0 0 8 5" fill="none" >
<Path d="M3.72536 4.37536C3.63647 4.37536 3.55314 4.36147 3.47536 4.3337C3.39758 4.30592 3.32536 4.2587 3.2587 4.19203L0.175362 1.1087C0.0531401 0.986473 -0.00519324 0.833696 0.000362319 0.650362C0.00591788 0.467029 0.0698068 0.314251 0.192029 0.192029C0.314251 0.0698068 0.469807 0.00869565 0.658696 0.00869565C0.847585 0.00869565 1.00314 0.0698068 1.12536 0.192029L3.72536 2.79203L6.34203 0.175362C6.46425 0.0531401 6.61703 -0.00519324 6.80036 0.000362319C6.9837 0.00591788 7.13647 0.0698068 7.2587 0.192029C7.38092 0.314251 7.44203 0.469807 7.44203 0.658696C7.44203 0.847585 7.38092 1.00314 7.2587 1.12536L4.19203 4.19203C4.12536 4.2587 4.05314 4.30592 3.97536 4.3337C3.89758 4.36147 3.81425 4.37536 3.72536 4.37536Z" fill="#09111F"/>
</Svg>

          					</View>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <____divider type={"horizontal"} /> */}
      			<View style={styles.____divider}>
        				<View style={styles.________linewrapper}>
<Svg style={styles.________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

        				</View>
        				<View style={styles._________linewrapper}>
<Svg style={styles._________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

        				</View>
      			</View>
      			<View style={styles.shippingaddress}>
        				<View style={styles._header}>
          					<Text style={styles._________title}>
            						{`Shipping address`}
          					</Text>
        				</View>
        				<View style={styles.content}>
          					<Text style={styles.placename}>
            						{`Home`}
          					</Text>
          					<Text style={styles.address}>
            						{`Ronald Richards\n(406) 555-0120\n2464 Royal Ln. Mesa, New Jersey 45463`}
          					</Text>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <_____divider type={"horizontal"} /> */}
      			<View style={styles._____divider}>
        				<View style={styles.__________linewrapper}>
<Svg style={styles.__________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

        				</View>
        				<View style={styles.___________linewrapper}>
<Svg style={styles.___________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

        				</View>
      			</View>
      			<View style={styles._______item}>
        				<View style={styles.products}>
          					<View style={styles.product}>
            						{/* Vigma RN:: can be replaced with <Card type={"product"} orientation={"horizontal"} size={"small"} /> */}
            						<View style={styles.card}>
              							<ImageBackground style={styles.image} source={{uri: /* dummy image */ 'https://dummyimage.com/56x56/000/fff.jpg'}}/>
              							<View style={styles.cardbody}>
                								<Text style={styles.__________title}>
                  									{`Single Drawer Bedside Table`}
                								</Text>
                								{/* Vigma RN:: can be replaced with <Price size={"small"} /> */}
                								<View style={styles.price}>
                  									<Text style={styles.currentprice}>
                    										{`Rp 600.000`}
                  									</Text>
                  									<View style={styles.initialpricewrapper}>
                    										<Text style={styles.initialprice}>
                      											{`Rp 800.000`}
                    										</Text>
                  									</View>
                								</View>
              							</View>
            						</View>
            						{/* Vigma RN:: can be replaced with <Labelvariationandquantity color={"grey"} /> */}
            						<View style={styles.labelvariationandquantity}>
              							<View style={styles.left}>
                								<Text style={styles.label}>
                  									{`Variation: White`}
                								</Text>
              							</View>
              							<View style={styles.right}>
                								<Text style={styles.labelright}>
                  									{`x2`}
                								</Text>
              							</View>
            						</View>
          					</View>
          					<View style={styles._product}>
            						{/* Vigma RN:: can be replaced with <_card type={"product"} orientation={"horizontal"} size={"small"} /> */}
            						<View style={styles._card}>
              							<ImageBackground style={styles._image} source={{uri: /* dummy image */ 'https://dummyimage.com/56x56/000/fff.jpg'}}/>
              							<View style={styles._cardbody}>
                								<Text style={styles.___________title}>
                  									{`Ceramic Cabinet`}
                								</Text>
                								{/* Vigma RN:: can be replaced with <_price size={"small"} /> */}
                								<View style={styles._price}>
                  									<Text style={styles._currentprice}>
                    										{`Rp 2.800.000`}
                  									</Text>
                  									<View style={styles._initialpricewrapper}>
                    										<Text style={styles._initialprice}>
                      											{`Rp 4.000.000`}
                    										</Text>
                  									</View>
                								</View>
              							</View>
            						</View>
            						{/* Vigma RN:: can be replaced with <_labelvariationandquantity color={"grey"} /> */}
            						<View style={styles._labelvariationandquantity}>
              							<View style={styles._left}>
                								<Text style={styles._label}>
                  									{`Variation: Black`}
                								</Text>
              							</View>
              							<View style={styles._right}>
                								<Text style={styles._labelright}>
                  									{`x1`}
                								</Text>
              							</View>
            						</View>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <______divider type={"horizontal"} /> */}
        				<View style={styles.______divider}>
          					<View style={styles.____________linewrapper}>
<Svg style={styles.____________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

          					</View>
          					<View style={styles._____________linewrapper}>
<Svg style={styles._____________line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <Labelsubtotal color={"dark"} /> */}
        				<View style={styles.labelsubtotal}>
          					<View style={styles.__left}>
            						<Text style={styles.__label}>
              							{`Total payment`}
            						</Text>
          					</View>
          					<View style={styles.__right}>
            						<Text style={styles.__labelright}>
              							{`Rp 4.060.000`}
            						</Text>
            						{/* Vigma RN:: can be replaced with <Iconright  /> */}
            						<View style={styles.iconright}>
              							<View style={styles.____________boundingbox}/>
<Svg style={styles.____________vector} width="8" height="5" viewBox="0 0 8 5" fill="none" >
<Path d="M3.72536 4.37536C3.63647 4.37536 3.55314 4.36147 3.47536 4.3337C3.39758 4.30592 3.32536 4.2587 3.2587 4.19203L0.175362 1.1087C0.0531401 0.986473 -0.00519324 0.833696 0.000362319 0.650362C0.00591788 0.467029 0.0698068 0.314251 0.192029 0.192029C0.314251 0.0698068 0.469807 0.00869565 0.658696 0.00869565C0.847585 0.00869565 1.00314 0.0698068 1.12536 0.192029L3.72536 2.79203L6.34203 0.175362C6.46425 0.0531401 6.61703 -0.00519324 6.80036 0.000362319C6.9837 0.00591788 7.13647 0.0698068 7.2587 0.192029C7.38092 0.314251 7.44203 0.469807 7.44203 0.658696C7.44203 0.847585 7.38092 1.00314 7.2587 1.12536L4.19203 4.19203C4.12536 4.2587 4.05314 4.30592 3.97536 4.3337C3.89758 4.36147 3.81425 4.37536 3.72536 4.37536Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Navbottom type={"buttons"} screen={"mobile"} /> */}
      			<View style={styles.navbottom}>
        				{/* Vigma RN:: can be replaced with <Buttonleft type={"outline"} color={"dark"} size={"medium"} state={"default"} /> */}
        				<View style={styles.buttonleft}>
          					<View style={styles._textwrapper}>
            						<Text style={styles.__text}>
              							{`Rate`}
            						</Text>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <Buttonright type={"filled"} color={"dark"} size={"medium"} state={"default"} /> */}
        				<View style={styles.buttonright}>
          					<View style={styles.__textwrapper}>
            						<Text style={styles.___text}>
              							{`Buy again`}
            						</Text>
          					</View>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Navtop type={"default"} screen={"mobile"} /> */}
      			<View style={styles.navtop}>
        				<View style={styles.itemleftwrapper}>
          					{/* Vigma RN:: can be replaced with <Itemleft state={"default"} /> */}
          					<View style={styles.itemleft}>
            						{/* Vigma RN:: can be replaced with <IconsOutlineArrow_back  /> */}
            						<View style={styles.iconsOutlineArrow_back}>
              							<View style={styles._____________boundingbox}/>
<Svg style={styles._____________vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.0625 13.0834L0.562503 7.58343C0.479169 7.50009 0.420141 7.40981 0.385419 7.31259C0.350697 7.21537 0.333336 7.1112 0.333336 7.00009C0.333336 6.88898 0.350697 6.78481 0.385419 6.68759C0.420141 6.59037 0.479169 6.50009 0.562503 6.41676L6.0625 0.916758C6.21528 0.76398 6.40625 0.684119 6.63542 0.677174C6.86459 0.67023 7.0625 0.750091 7.22917 0.916758C7.39584 1.06954 7.48264 1.26051 7.48959 1.48967C7.49653 1.71884 7.41667 1.91676 7.25 2.08342L3.16667 6.16676H12.4792C12.7153 6.16676 12.9132 6.24662 13.0729 6.40634C13.2326 6.56606 13.3125 6.76398 13.3125 7.00009C13.3125 7.2362 13.2326 7.43412 13.0729 7.59384C12.9132 7.75356 12.7153 7.83343 12.4792 7.83343H3.16667L7.25 11.9168C7.40278 12.0695 7.48264 12.264 7.48959 12.5001C7.49653 12.7362 7.41667 12.9306 7.25 13.0834C7.09723 13.2501 6.90278 13.3334 6.66667 13.3334C6.43056 13.3334 6.22917 13.2501 6.0625 13.0834Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
        				<Text style={styles.____________title}>
          					{`Order Details`}
        				</Text>
        				<View style={styles.itemrightwrapper}/>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	orderDetails: {
      flexShrink: 0,
      paddingTop: 0,
      paddingBottom: 88,
      backgroundColor: "rgba(255, 255, 255, 1)",
      rowGap: 0,
      paddingHorizontal: 0,
},
  	orderstatus: {
    flexShrink: 0,
    rowGap: 24
},
  	message: {
    flexShrink: 0,
    paddingTop: 16,
    paddingBottom: 20,
    backgroundColor: "rgba(8, 199, 84, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 12,
    paddingHorizontal: 24
},
  	iconsOutlineOrder_approve: {
    flexShrink: 0,
    height: 32,
    width: 32,
    
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 1,
    bottom: 1,
    left: 4,
    overflow: "visible"
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	steps: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-end",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24
},
  	payed: {
    flexShrink: 0,
    alignItems: "center",
    rowGap: 8
},
  	iconsOutlineAccount_balance_wallet: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 3,
    left: 3,
    overflow: "visible"
},
  	iconsFilledCheck_circle: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	__boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	__vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
},
  	divider: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	_line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	packed: {
    flexShrink: 0,
    alignItems: "center",
    rowGap: 8
},
  	iconsOutlinePackage: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible"
},
  	_iconsFilledCheck_circle: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
},
  	_divider: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	__linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	__line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	___linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	___line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	deliver: {
    flexShrink: 0,
    alignItems: "center",
    rowGap: 8
},
  	iconsOutlineLocal_shipping: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	_____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 1,
    bottom: 4,
    left: 1,
    overflow: "visible"
},
  	__iconsFilledCheck_circle: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
},
  	__divider: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	____linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	____line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_____linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	_____line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	delivered: {
    flexShrink: 0,
    alignItems: "center",
    rowGap: 8
},
  	iconsOutlineDoorbell_3p: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	_______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 5,
    bottom: 2,
    left: 5,
    overflow: "visible"
},
  	___iconsFilledCheck_circle: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
},
  	___divider: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	______linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	______line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_______linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 0
},
  	_______line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	completed: {
    flexShrink: 0,
    alignItems: "center",
    rowGap: 8
},
  	_iconsOutlineOrder_approve: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	_________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 1,
    bottom: 1,
    left: 3,
    overflow: "visible"
},
  	____iconsFilledCheck_circle: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	__________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	__________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
},
  	shippinginformation: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 24,
    paddingBottom: 12,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 24
},
  	header: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 12,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	_title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	items: {
    flexShrink: 0,
    height: 152,
    alignItems: "flex-start",
    rowGap: 8
},
  	item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	__title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	_item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	___title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	__subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	__item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	____title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	___subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	___item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	_____title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	____subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	____item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	______title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_____subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	_____item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	_______title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	______subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	______item: {
    flexShrink: 0,
    width: 312,
    alignItems: "flex-start",
    rowGap: 0
},
  	________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_______subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	buttonviewmore: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0
},
  	textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0
},
  	_text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	___________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	___________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 6,
    right: 5,
    bottom: 6,
    left: 4,
    overflow: "visible"
},
  	____divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24
},
  	________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	_________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	shippingaddress: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 8,
    paddingBottom: 12,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24
},
  	_header: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0
},
  	_________title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	content: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0
},
  	placename: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	address: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_____divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24
},
  	__________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	__________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	___________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	___________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_______item: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 12,
    paddingBottom: 24,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0,
    paddingHorizontal: 24
},
  	products: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 12
},
  	product: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
},
  	card: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 12,
    borderRadius: 12
},
  	image: {
    flexShrink: 0,
    width: 56,
    height: 56,
    borderRadius: 8
},
  	cardbody: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 2
},
  	__________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	currentprice: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	initialpricewrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through"
},
  	labelvariationandquantity: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_product: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
},
  	_card: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 12,
    borderRadius: 12
},
  	_image: {
    flexShrink: 0,
    width: 56,
    height: 56,
    borderRadius: 8
},
  	_cardbody: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 2
},
  	___________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	_price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	_currentprice: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_initialpricewrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through"
},
  	_labelvariationandquantity: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	_left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	_right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	_labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	______divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	____________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	____________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_____________linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	_____________line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	labelsubtotal: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	__left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	__label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	__right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	__labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	iconright: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	____________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	____________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 6,
    right: 5,
    bottom: 6,
    left: 4,
    overflow: "visible"
},
  	navbottom: {
    position: "absolute",
    flexShrink: 0,
    bottom: 0,
    left: 0,
    right: 0,
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 12,
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	buttonleft: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
},
  	_textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	__text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	buttonright: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    backgroundColor: "rgba(9, 17, 31, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 12
},
  	__textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	___text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	navtop: {
    position: "absolute",
    flexShrink: 0,
    left: 0,
    right: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 14,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	itemleftwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
},
  	itemleft: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	iconsOutlineArrow_back: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_____________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_____________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 3,
    overflow: "visible"
},
  	____________title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	itemrightwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
}
})