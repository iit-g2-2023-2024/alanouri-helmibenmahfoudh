import { router } from 'expo-router';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Path } from 'react-native-svg';

export default function OrderSuccessful() {
  const orederDetaillsButtonClick = () => {
    router.push({pathname: "order/orderDetails"}) 
  };
  const homeButtonClick = () => {
    router.push({pathname: "(tabs)/home"}) 
  };
    return (
    		<View style={styles.orderSuccessful}>
      			<View style={styles.conteiner}>
        				<View style={styles.message}>
          					{/* Vigma RN:: can be replaced with <Icon  /> */}
          					<View style={styles.icon}>
            						<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="100" height="96" viewBox="0 0 100 96" fill="none" >
<Path d="M32.0333 93.5001L25.2667 82.0667L12.4333 79.2667C11.2667 79.0334 10.3334 78.4306 9.63335 77.4584C8.93335 76.4862 8.66113 75.4167 8.81668 74.2501L10.1 61.0667L1.35002 51.0334C0.572238 50.1778 0.18335 49.1667 0.18335 48.0001C0.18335 46.8334 0.572238 45.8223 1.35002 44.9667L10.1 34.9334L8.81668 21.7501C8.66113 20.5834 8.93335 19.514 9.63335 18.5417C10.3334 17.5695 11.2667 16.9667 12.4333 16.7334L25.2667 13.9334L32.0333 2.50007C32.6556 1.48895 33.5111 0.808398 34.6 0.458398C35.6889 0.108398 36.7778 0.166732 37.8667 0.633398L50 5.76673L62.1334 0.633398C63.2222 0.166732 64.3111 0.108398 65.4 0.458398C66.4889 0.808398 67.3445 1.48895 67.9667 2.50007L74.7334 13.9334L87.5667 16.7334C88.7333 16.9667 89.6667 17.5695 90.3667 18.5417C91.0667 19.514 91.3389 20.5834 91.1833 21.7501L89.9 34.9334L98.65 44.9667C99.4278 45.8223 99.8167 46.8334 99.8167 48.0001C99.8167 49.1667 99.4278 50.1778 98.65 51.0334L89.9 61.0667L91.1833 74.2501C91.3389 75.4167 91.0667 76.4862 90.3667 77.4584C89.6667 78.4306 88.7333 79.0334 87.5667 79.2667L74.7334 82.0667L67.9667 93.5001C67.3445 94.5112 66.4889 95.1917 65.4 95.5417C64.3111 95.8917 63.2222 95.8334 62.1334 95.3667L50 90.2334L37.8667 95.3667C36.7778 95.8334 35.6889 95.8917 34.6 95.5417C33.5111 95.1917 32.6556 94.5112 32.0333 93.5001ZM45.1 51.2667L38.3333 44.6167C37.4778 43.7612 36.4083 43.3334 35.125 43.3334C33.8417 43.3334 32.7333 43.8001 31.8 44.7334C30.9445 45.589 30.5167 46.6778 30.5167 48.0001C30.5167 49.3223 30.9445 50.4112 31.8 51.2667L41.8333 61.3001C42.7667 62.2334 43.8556 62.7001 45.1 62.7001C46.3445 62.7001 47.4333 62.2334 48.3667 61.3001L68.2 41.4667C69.1333 40.5334 69.5806 39.4445 69.5417 38.2001C69.5028 36.9556 69.0556 35.8667 68.2 34.9334C67.2667 34.0001 66.1583 33.514 64.875 33.4751C63.5917 33.4362 62.4833 33.8834 61.55 34.8167L45.1 51.2667Z" fill="#0856DB"/>
</Svg>

          					</View>
          					<View style={styles.text}>
            						<Text style={styles.title}>
              							{`Order Successful!`}
            						</Text>
            						<Text style={styles.subtitle}>
              							{`Thank you for your purchase. You will get a shipping confirmation soon.`}
            						</Text>
          					</View>
        				</View>
      			</View>
      			<View style={styles.actions}>
        				{/* Vigma RN:: can be replaced with <Buttonvieworder type={"filled"} color={"dark"} size={"medium"} state={"default"} /> */}
        				<View style={styles.buttonvieworder}>
          					<View style={styles.textwrapper}>
            						<Text style={styles._text} onPress={orederDetaillsButtonClick}>
              							{`View order details`}
            						</Text>
          					</View>
        				</View>
        				{/* Vigma RN:: can be replaced with <Buttoncontinueshopping type={"outline"} color={"dark"} size={"medium"} state={"default"} /> */}
        				<View style={styles.buttoncontinueshopping}>
          					<View style={styles._textwrapper}>
            						<Text style={styles.__text} onPress={homeButtonClick}>
              							{`Continue shopping`}
            						</Text>
          					</View>
        				</View>
      			</View>
    		</View>
    )
}

const styles = StyleSheet.create({
  	orderSuccessful: {
    flexShrink: 0,
    height: 844,
    width: "auto",
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 0
},
  	conteiner: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "center",
    justifyContent: "center",
    rowGap: 48,
    padding: 24
},
  	message: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "center",
    justifyContent: "center",
    rowGap: 48
},
  	icon: {
    flexShrink: 0,
    height: 112,
    width: 112,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 8,
    right: 6,
    bottom: 8,
    left: 6,
    overflow: "visible"
},
  	text: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 12
},
  	title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "center",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 24,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 32
},
  	subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "center",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 24
},
  	actions: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 12,
    padding: 24
},
  	buttonvieworder: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(9, 17, 31, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 12
},
  	textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	_text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	buttoncontinueshopping: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
},
  	_textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	__text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
}
})