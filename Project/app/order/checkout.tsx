import { router } from 'expo-router';
import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';
import { Svg, Path, Line } from 'react-native-svg';

export default function Checkout() {
    const payementButtonClick = () => {
        router.push({pathname: "order/payementConfirmation"}) 
      };
    return (
    		<View style={styles.checkout}>
      			<View style={styles.shippingaddress}>
        				<View style={styles.header}>
          					<Text style={styles.title}>
            						{`Shipping address`}
          					</Text>
          					{/* Vigma RN:: can be replaced with <Buttoneditaddress type={"filledCircle"} color={"light"} size={"small"} state={"default"} /> */}
          					<View style={styles.buttoneditaddress}>
            						{/* Vigma RN:: can be replaced with <Icon  /> */}
            						<View style={styles.icon}>
              							<View style={styles.boundingbox}/>
<Svg style={styles.vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M1.33333 13.8C0.966667 13.8 0.652778 13.6695 0.391667 13.4084C0.130556 13.1473 0 12.8334 0 12.4667V3.13337C0 2.7667 0.130556 2.45281 0.391667 2.1917C0.652778 1.93059 0.966667 1.80003 1.33333 1.80003H7.28333L5.95 3.13337H1.33333V12.4667H10.6667V7.83337L12 6.50003V12.4667C12 12.8334 11.8694 13.1473 11.6083 13.4084C11.3472 13.6695 11.0333 13.8 10.6667 13.8H1.33333ZM8.78333 2.18337L9.73333 3.1167L5.33333 7.5167V8.4667H6.26667L10.6833 4.05003L11.6333 4.98337L7.21667 9.40003C7.09444 9.52225 6.95278 9.61948 6.79167 9.6917C6.63056 9.76392 6.46111 9.80003 6.28333 9.80003H4.66667C4.47778 9.80003 4.31944 9.73614 4.19167 9.60837C4.06389 9.48059 4 9.32225 4 9.13337V7.5167C4 7.33892 4.03333 7.16948 4.1 7.00837C4.16667 6.84725 4.26111 6.70559 4.38333 6.58337L8.78333 2.18337ZM11.6333 4.98337L8.78333 2.18337L10.45 0.516699C10.7167 0.250033 11.0361 0.116699 11.4083 0.116699C11.7806 0.116699 12.0944 0.250033 12.35 0.516699L13.2833 1.4667C13.5389 1.72225 13.6667 2.03337 13.6667 2.40003C13.6667 2.7667 13.5389 3.07781 13.2833 3.33337L11.6333 4.98337Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
        				<View style={styles.content}>
          					<Text style={styles.placename}>
            						{`Home`}
          					</Text>
          					<Text style={styles.address}>
            						{`Ronald Richards\n(406) 555-0120\n2464 Royal Ln. Mesa, New Jersey 45463`}
          					</Text>
        				</View>
      			</View>
      			<View style={styles.checkoutitems}>
        				<View style={styles.item}>
          					<View style={styles.product}>
            						{/* Vigma RN:: can be replaced with <Card type={"product"} orientation={"horizontal"} size={"small"} /> */}
            						<View style={styles.card}>
              							<ImageBackground style={styles.image} source={{uri: /* dummy image */ 'https://dummyimage.com/56x56/000/fff.jpg'}}/>
              							<View style={styles.cardbody}>
                								<Text style={styles._title}>
                  									{`Single Drawer Bedside Table`}
                								</Text>
                								{/* Vigma RN:: can be replaced with <Price size={"small"} /> */}
                								<View style={styles.price}>
                  									<Text style={styles.currentprice}>
                    										{`Rp 600.000`}
                  									</Text>
                  									<View style={styles.initialpricewrapper}>
                    										<Text style={styles.initialprice}>
                      											{`Rp 800.000`}
                    										</Text>
                  									</View>
                								</View>
              							</View>
            						</View>
            						{/* Vigma RN:: can be replaced with <Labelvariationandquantity color={"grey"} /> */}
            						<View style={styles.labelvariationandquantity}>
              							<View style={styles.left}>
                								<Text style={styles.label}>
                  									{`Variation: White`}
                								</Text>
              							</View>
              							<View style={styles.right}>
                								<Text style={styles.labelright}>
                  									{`x2`}
                								</Text>
              							</View>
            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <Divider type={"horizontal"} /> */}
          					<View style={styles.divider}>
            						<View style={styles.linewrapper}>
<Svg style={styles.line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

            						</View>
            						<View style={styles._linewrapper}>
<Svg style={styles._line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <Labelsubtotal color={"dark"} /> */}
          					<View style={styles.labelsubtotal}>
            						<View style={styles._left}>
              							<Text style={styles._label}>
                								{`Subtotal`}
              							</Text>
            						</View>
            						<View style={styles._right}>
              							<Text style={styles._labelright}>
                								{`Rp 1.200.000`}
              							</Text>
            						</View>
          					</View>
        				</View>
        				<View style={styles._item}>
          					<View style={styles._product}>
            						{/* Vigma RN:: can be replaced with <_card type={"product"} orientation={"horizontal"} size={"small"} /> */}
            						<View style={styles._card}>
              							<ImageBackground style={styles._image} source={{uri: /* dummy image */ 'https://dummyimage.com/56x56/000/fff.jpg'}}/>
              							<View style={styles._cardbody}>
                								<Text style={styles.__title}>
                  									{`Ceramic Cabinet`}
                								</Text>
                								{/* Vigma RN:: can be replaced with <_price size={"small"} /> */}
                								<View style={styles._price}>
                  									<Text style={styles._currentprice}>
                    										{`Rp 2.800.000`}
                  									</Text>
                  									<View style={styles._initialpricewrapper}>
                    										<Text style={styles._initialprice}>
                      											{`Rp 4.000.000`}
                    										</Text>
                  									</View>
                								</View>
              							</View>
            						</View>
            						{/* Vigma RN:: can be replaced with <_labelvariationandquantity color={"grey"} /> */}
            						<View style={styles._labelvariationandquantity}>
              							<View style={styles.__left}>
                								<Text style={styles.__label}>
                  									{`Variation: Black`}
                								</Text>
              							</View>
              							<View style={styles.__right}>
                								<Text style={styles.__labelright}>
                  									{`x1`}
                								</Text>
              							</View>
            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <_divider type={"horizontal"} /> */}
          					<View style={styles._divider}>
            						<View style={styles.__linewrapper}>
<Svg style={styles.__line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

            						</View>
            						<View style={styles.___linewrapper}>
<Svg style={styles.___line} width="156" height="1" viewBox="0 0 156 1" fill="none" >
<Line y1="0.5" x2="156" y2="0.5" stroke="#E1E5EB"/>
</Svg>

            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <_labelsubtotal color={"dark"} /> */}
          					<View style={styles._labelsubtotal}>
            						<View style={styles.___left}>
              							<Text style={styles.___label}>
                								{`Subtotal`}
              							</Text>
            						</View>
            						<View style={styles.___right}>
              							<Text style={styles.___labelright}>
                								{`Rp 2.800.000`}
              							</Text>
            						</View>
          					</View>
        				</View>
      			</View>
      			<View style={styles.paymentmethod}>
        				{/* Vigma RN:: can be replaced with <ListListitem state={"default"} /> */}
        				<View style={styles.listListitem}>
          					<View style={styles.text}>
            						<Text style={styles.___title}>
              							{`Payment method`}
            						</Text>
          					</View>
          					<View style={styles.itemright}>
            						<View style={styles.badgewrapper}>
              							{/* Vigma RN:: can be replaced with <Badge type={"text"} size={"small"} /> */}
              							<View style={styles.badge}>
                								<Text style={styles.____label}>
                  									{`Gopay`}
                								</Text>
              							</View>
            						</View>
            						{/* Vigma RN:: can be replaced with <Iconright  /> */}
            						<View style={styles.iconright}>
              							<View style={styles._boundingbox}/>
<Svg style={styles._vector} width="7" height="10" viewBox="0 0 7 10" fill="none" >
<Path d="M1.08332 9.41675C0.930545 9.26397 0.854156 9.06953 0.854156 8.83342C0.854156 8.5973 0.930545 8.40286 1.08332 8.25008L4.33332 5.00008L1.08332 1.75008C0.930545 1.5973 0.854156 1.40286 0.854156 1.16675C0.854156 0.930637 0.930545 0.736192 1.08332 0.583415C1.2361 0.430637 1.43055 0.354248 1.66666 0.354248C1.90277 0.354248 2.09721 0.430637 2.24999 0.583415L6.08332 4.41675C6.16666 4.50008 6.22568 4.59036 6.26041 4.68758C6.29513 4.7848 6.31249 4.88897 6.31249 5.00008C6.31249 5.11119 6.29513 5.21536 6.26041 5.31258C6.22568 5.4098 6.16666 5.50008 6.08332 5.58341L2.24999 9.41675C2.09721 9.56953 1.90277 9.64592 1.66666 9.64592C1.43055 9.64592 1.2361 9.56953 1.08332 9.41675Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
      			</View>
      			<View style={styles.paymentdetails}>
        				<View style={styles._header}>
          					<Text style={styles.____title}>
            						{`Payment details`}
          					</Text>
        				</View>
        				<View style={styles._content}>
          					{/* Vigma RN:: can be replaced with <__labelsubtotal color={"dark"} /> */}
          					<View style={styles.__labelsubtotal}>
            						<View style={styles.____left}>
              							<Text style={styles._____label}>
                								{`Subtotal`}
              							</Text>
            						</View>
            						<View style={styles.____right}>
              							<Text style={styles.____labelright}>
                								{`Rp 4.000.000`}
              							</Text>
            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <Labelshippingfee color={"dark"} /> */}
          					<View style={styles.labelshippingfee}>
            						<View style={styles._____left}>
              							<Text style={styles.______label}>
                								{`Shipping fee`}
              							</Text>
            						</View>
            						<View style={styles._____right}>
              							<Text style={styles._____labelright}>
                								{`Rp 50.000`}
              							</Text>
            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <Labelservicefee color={"dark"} /> */}
          					<View style={styles.labelservicefee}>
            						<View style={styles.______left}>
              							<Text style={styles._______label}>
                								{`Service fee`}
              							</Text>
            						</View>
            						<View style={styles.______right}>
              							<Text style={styles.______labelright}>
                								{`Rp 10.000`}
              							</Text>
            						</View>
          					</View>
          					{/* Vigma RN:: can be replaced with <_labelservicefee color={"dark"} /> */}
          					<View style={styles._labelservicefee}>
            						<View style={styles._______left}>
              							<Text style={styles.________label}>
                								{`Total payment`}
              							</Text>
            						</View>
            						<View style={styles._______right}>
              							<Text style={styles._______labelright}>
                								{`Rp 4.060.000`}
              							</Text>
            						</View>
          					</View>
        				</View>
      			</View>
      			{/* Vigma RN:: can be replaced with <Navtop type={"default"} screen={"mobile"} /> */}
      			<View style={styles.navtop}>
        				<View style={styles.itemleftwrapper}>
          					{/* Vigma RN:: can be replaced with <Itemleft state={"default"} /> */}
          					<View style={styles.itemleft}>
            						{/* Vigma RN:: can be replaced with <IconsOutlineArrow_back  /> */}
            						<View style={styles.iconsOutlineArrow_back}>
              							<View style={styles.__boundingbox}/>
<Svg style={styles.__vector} width="14" height="14" viewBox="0 0 14 14" fill="none" >
<Path d="M6.0625 13.0834L0.562495 7.58343C0.479162 7.50009 0.420134 7.40981 0.385412 7.31259C0.350689 7.21537 0.333328 7.1112 0.333328 7.00009C0.333328 6.88898 0.350689 6.78481 0.385412 6.68759C0.420134 6.59037 0.479162 6.50009 0.562495 6.41676L6.0625 0.916758C6.21527 0.76398 6.40625 0.684119 6.63541 0.677174C6.86458 0.67023 7.0625 0.750091 7.22916 0.916758C7.39583 1.06954 7.48263 1.26051 7.48958 1.48967C7.49652 1.71884 7.41666 1.91676 7.25 2.08342L3.16666 6.16676H12.4792C12.7153 6.16676 12.9132 6.24662 13.0729 6.40634C13.2326 6.56606 13.3125 6.76398 13.3125 7.00009C13.3125 7.2362 13.2326 7.43412 13.0729 7.59384C12.9132 7.75356 12.7153 7.83343 12.4792 7.83343H3.16666L7.25 11.9168C7.40277 12.0695 7.48263 12.264 7.48958 12.5001C7.49652 12.7362 7.41666 12.9306 7.25 13.0834C7.09722 13.2501 6.90277 13.3334 6.66666 13.3334C6.43055 13.3334 6.22916 13.2501 6.0625 13.0834Z" fill="#09111F"/>
</Svg>

            						</View>
          					</View>
        				</View>
        				<Text style={styles._____title}>
          					{`Checkout`}
        				</Text>
        				<View style={styles.itemrightwrapper}/>
      			</View>
      			<View style={styles.navcheckout}>
        				<View style={styles.totalpricewrapper}>
          					{/* Vigma RN:: can be replaced with <BaseInputlabel color={"white"} /> */}
          					<View style={styles.baseInputlabel}>
            						<View style={styles.________left}>
              							<Text style={styles._________label}>
                								{`Total payment`}
              							</Text>
            						</View>
            						<View style={styles.________right}/>
          					</View>
          					<Text style={styles.totalprice}>
            						{`Rp 4.060.000`}
          					</Text>
        				</View>
        				{/* Vigma RN:: can be replaced with <Buttoncheckout type={"filled"} color={"dark"} size={"large"} state={"default"} /> */}
        				<View style={styles.buttoncheckout}>
          					<View style={styles.textwrapper}>
            						<Text style={styles._text} onPress={payementButtonClick}>
              							{`Place order`}
            						</Text>
          					</View>
          					{/* Vigma RN:: can be replaced with <_iconright  /> */}
          					<View style={styles._iconright}>
            						<View style={styles.___boundingbox}/>
<Svg style={styles.___vector} width="16" height="16" viewBox="0 0 16 16" fill="none" >
<Path d="M7.3 15.3C7.11667 15.1167 7.02083 14.8834 7.0125 14.6C7.00417 14.3167 7.09167 14.0834 7.275 13.9L12.175 9.00005H1C0.716667 9.00005 0.479167 8.90422 0.2875 8.71255C0.0958333 8.52088 0 8.28338 0 8.00005C0 7.71672 0.0958333 7.47922 0.2875 7.28755C0.479167 7.09588 0.716667 7.00005 1 7.00005H12.175L7.275 2.10005C7.09167 1.91672 7.00417 1.68338 7.0125 1.40005C7.02083 1.11672 7.11667 0.883382 7.3 0.700049C7.48333 0.516715 7.71667 0.425049 8 0.425049C8.28333 0.425049 8.51667 0.516715 8.7 0.700049L15.3 7.30005C15.4 7.38338 15.4708 7.48755 15.5125 7.61255C15.5542 7.73755 15.575 7.86672 15.575 8.00005C15.575 8.13338 15.5542 8.25838 15.5125 8.37505C15.4708 8.49172 15.4 8.60005 15.3 8.70005L8.7 15.3C8.51667 15.4834 8.28333 15.575 8 15.575C7.71667 15.575 7.48333 15.4834 7.3 15.3Z" fill="white"/>
</Svg>

          					</View>
        				</View>
      			</View>
    		</View>
        
    )
}

const styles = StyleSheet.create({
    page: {
        backgroundColor: "white",
      },
  	checkout: {  
      height: "auto",
        alignSelf: "stretch",
        flexShrink: 0,
        alignItems: "center",
        rowGap: 12,
        paddingVertical: 12,
        paddingHorizontal: 24,
},
  	shippingaddress: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    alignItems: "flex-start",
    rowGap: 12
},
  	header: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0
},
  	title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	buttoneditaddress: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999
},
  	icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
},
  	boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 0,
    bottom: 0,
    left: 2,
    overflow: "visible"
},
  	content: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 0,
    paddingRight: 8,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 0
},
  	placename: {
    flexShrink: 0,
    width: 320,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	address: {
    flexShrink: 0,
    width: 320,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	checkoutitems: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 24,
    paddingVertical: 12,
    paddingHorizontal: 24
},
  	item: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	product: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
},
  	card: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 12,
    borderRadius: 12
},
  	image: {
    flexShrink: 0,
    width: 56,
    height: 56,
    borderRadius: 8
},
  	cardbody: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 2
},
  	_title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(8, 86, 219, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	currentprice: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	initialpricewrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through"
},
  	labelvariationandquantity: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	_line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	labelsubtotal: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	_left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	_labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_item: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	_product: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
},
  	_card: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 12,
    borderRadius: 12
},
  	_image: {
    flexShrink: 0,
    width: 56,
    height: 56,
    borderRadius: 8
},
  	_cardbody: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 2
},
  	__title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(8, 86, 219, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	_price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0
},
  	_currentprice: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_initialpricewrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through"
},
  	_labelvariationandquantity: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	__left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	__label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16
},
  	__right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	__labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	_divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
},
  	__linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	__line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	___linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0
},
  	___line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible"
},
  	_labelsubtotal: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	___left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	___label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	___right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	___labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	paymentmethod: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24
},
  	listListitem: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 16,
    paddingRight: 10,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 8,
    paddingVertical: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
},
  	text: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0
},
  	___title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	itemright: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0
},
  	badgewrapper: {
    flexShrink: 0,
    paddingLeft: 0,
    paddingRight: 4,
    alignItems: "flex-start",
    justifyContent: "center",
    rowGap: 0,
    paddingVertical: 0
},
  	badge: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    borderRadius: 4
},
  	____label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	iconright: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	_boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	_vector: {
    position: "absolute",
    flexShrink: 0,
    top: 5,
    right: 7,
    bottom: 5,
    left: 8,
    overflow: "visible"
},
  	paymentdetails: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 6,
    paddingBottom: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24
},
  	_header: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0
},
  	____title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	_content: {
    marginBottom : 200,
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 4
},
  	__labelsubtotal: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	____left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_____label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	____right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	____labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	labelshippingfee: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	_____left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	______label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_____right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	_____labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	labelservicefee: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	______left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_______label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	______right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	______labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
},
  	_labelservicefee: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
},
  	_______left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	_______right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	_______labelright: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	navtop: {
    position: "absolute",
    flexShrink: 0,
    left: 0,
    right: 0,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 14,
    borderColor: "rgba(225, 229, 235, 1)"
},
  	itemleftwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
},
  	itemleft: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999
},
  	iconsOutlineArrow_back: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0
},
  	__boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	__vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 3,
    overflow: "visible"
},
  	_____title: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	itemrightwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0
},
  	navcheckout: {
    position: "absolute",
    flexShrink: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(8, 86, 219, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 24,
    paddingVertical: 14,
    paddingHorizontal: 24
},
  	totalpricewrapper: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0
},
  	baseInputlabel: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 12
},
  	________left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
},
  	_________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(225, 229, 235, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
},
  	________right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
},
  	totalprice: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(240, 242, 245, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
},
  	buttoncheckout: {
    flexShrink: 0,
    backgroundColor: "rgba(8, 86, 219, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 8,
    paddingVertical: 6,
    paddingHorizontal: 0,
    borderRadius: 12
},
  	textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
},
  	_text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
},
  	_iconright: {
    flexShrink: 0,
    height: 24,
    width: 24,
    alignItems: "flex-start",
    rowGap: 0
},
  	___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)"
},
  	___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 4,
    overflow: "visible"
}
})