import { AntDesign } from "@expo/vector-icons";
import { router } from "expo-router";
import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  Modal,
} from "react-native";
import { Svg, Path } from "react-native-svg";
import { useDispatch } from "react-redux";
import { service } from "../../service";
import CardProductDetail from "../../components/CardDetailProduct";

const SearchResult = () => {
  const [firstSixElements, setFirstSixElements] = useState<any[]>([]);
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    console.log("init home");
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      const { data } = await service.products.productsList();
      dispatch({ type: "SET_PRODUCTS", payload: data.paginatedResult });
      //console.log(data);
      const parsedArray = data.paginatedResult.map((item) => {
        // Assuming 'value' property is a stringified JSON
        const parsedValue =
          item.response && item.response.value
            ? JSON.parse(item.response.value)
            : null;

        return {
          ...item,
          response: {
            ...item.response,
            value: parsedValue,
          },
        };
      });
      setFirstSixElements(parsedArray.slice(0, 6));
      console.log(firstSixElements);
    };

    fetchData();
  }, []);
  return (
    <View>
      <View style={styles.navbar}>
        {/* Vigma RN:: can be replaced with <Navtop type={"form"} screen={"mobile"} /> */}
        <View style={styles.navtop}>
          <View style={styles.itemleftwrapper}>
            {/* Vigma RN:: can be replaced with <Itemleft state={"default"} /> */}
            <View style={styles.itemleft}>
              {/* Vigma RN:: can be replaced with <IconsOutlineArrow_back  /> */}
              <View style={styles.iconsOutlineArrow_back}>
                <TouchableOpacity
                  style={styles.boundingbox}
                  onPress={() => router.back()}
                >
                  <AntDesign name="arrowleft" size={24} color="black" />
                </TouchableOpacity>
                <View />
              </View>
            </View>
          </View>
          <View style={styles.inputwrapper}>
            {/* Vigma RN:: can be replaced with <Inputfield size={"medium"} state={"filled"} /> */}
            <View style={styles.inputfield}>
              {/* Vigma RN:: can be replaced with <Iconleft  /> */}
              <View style={styles.iconleft}>
                <View style={styles._boundingbox} />
                <TouchableOpacity>
                  <Svg
                    style={styles._vector}
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                  >
                    <Path
                      d="M13.75 14.9167L9.08333 10.25C8.66667 10.5833 8.1875 10.8472 7.64583 11.0417C7.10417 11.2361 6.52778 11.3333 5.91667 11.3333C4.40278 11.3333 3.12153 10.809 2.07292 9.76042C1.02431 8.71181 0.5 7.43056 0.5 5.91667C0.5 4.40278 1.02431 3.12153 2.07292 2.07292C3.12153 1.02431 4.40278 0.5 5.91667 0.5C7.43055 0.5 8.71181 1.02431 9.76042 2.07292C10.809 3.12153 11.3333 4.40278 11.3333 5.91667C11.3333 6.52778 11.2361 7.10417 11.0417 7.64583C10.8472 8.1875 10.5833 8.66667 10.25 9.08333L14.9375 13.7708C15.0903 13.9236 15.1667 14.1111 15.1667 14.3333C15.1667 14.5556 15.0833 14.75 14.9167 14.9167C14.7639 15.0694 14.5694 15.1458 14.3333 15.1458C14.0972 15.1458 13.9028 15.0694 13.75 14.9167ZM5.91667 9.66667C6.95833 9.66667 7.84375 9.30208 8.57292 8.57292C9.30208 7.84375 9.66667 6.95833 9.66667 5.91667C9.66667 4.875 9.30208 3.98958 8.57292 3.26042C7.84375 2.53125 6.95833 2.16667 5.91667 2.16667C4.875 2.16667 3.98958 2.53125 3.26042 3.26042C2.53125 3.98958 2.16667 4.875 2.16667 5.91667C2.16667 6.95833 2.53125 7.84375 3.26042 8.57292C3.98958 9.30208 4.875 9.66667 5.91667 9.66667Z"
                      fill="#919BAD"
                    />
                  </Svg>
                </TouchableOpacity>
              </View>
              <View style={styles.inputvaluewrapper}>
                <TextInput
                  style={styles.inputvalue}
                  placeholder="Search"
                  value="Table"
                ></TextInput>
              </View>
            </View>
          </View>
          <View style={styles.itemrightwrapper}>
            {/* Vigma RN:: can be replaced with <Itemright state={"default"} /> */}
            <View style={styles.itemright}>
              {/* Vigma RN:: can be replaced with <IconsOutlineTune  /> */}
              <View style={styles.iconsOutlineTune}>
                <View style={styles.__boundingbox} />
                <TouchableOpacity
                  onPress={() => {
                    setShowModal(true);
                  }}
                >
                  <Svg
                    style={styles.__vector}
                    width="16"
                    height="16"
                    viewBox="0 0 16 16"
                    fill="none"
                  >
                    <Path
                      d="M1.33333 13.8333C1.09722 13.8333 0.899306 13.7535 0.739583 13.5938C0.579861 13.434 0.5 13.2361 0.5 13C0.5 12.7639 0.579861 12.566 0.739583 12.4062C0.899306 12.2465 1.09722 12.1667 1.33333 12.1667H4.66667C4.90278 12.1667 5.10069 12.2465 5.26042 12.4062C5.42014 12.566 5.5 12.7639 5.5 13C5.5 13.2361 5.42014 13.434 5.26042 13.5938C5.10069 13.7535 4.90278 13.8333 4.66667 13.8333H1.33333ZM1.33333 3.83333C1.09722 3.83333 0.899306 3.75347 0.739583 3.59375C0.579861 3.43403 0.5 3.23611 0.5 3C0.5 2.76389 0.579861 2.56597 0.739583 2.40625C0.899306 2.24653 1.09722 2.16667 1.33333 2.16667H8C8.23611 2.16667 8.43403 2.24653 8.59375 2.40625C8.75347 2.56597 8.83333 2.76389 8.83333 3C8.83333 3.23611 8.75347 3.43403 8.59375 3.59375C8.43403 3.75347 8.23611 3.83333 8 3.83333H1.33333ZM8 15.5C7.76389 15.5 7.56597 15.4201 7.40625 15.2604C7.24653 15.1007 7.16667 14.9028 7.16667 14.6667V11.3333C7.16667 11.0972 7.24653 10.8993 7.40625 10.7396C7.56597 10.5799 7.76389 10.5 8 10.5C8.23611 10.5 8.43403 10.5799 8.59375 10.7396C8.75347 10.8993 8.83333 11.0972 8.83333 11.3333V12.1667H14.6667C14.9028 12.1667 15.1007 12.2465 15.2604 12.4062C15.4201 12.566 15.5 12.7639 15.5 13C15.5 13.2361 15.4201 13.434 15.2604 13.5938C15.1007 13.7535 14.9028 13.8333 14.6667 13.8333H8.83333V14.6667C8.83333 14.9028 8.75347 15.1007 8.59375 15.2604C8.43403 15.4201 8.23611 15.5 8 15.5ZM4.66667 10.5C4.43056 10.5 4.23264 10.4201 4.07292 10.2604C3.91319 10.1007 3.83333 9.90278 3.83333 9.66667V8.83333H1.33333C1.09722 8.83333 0.899306 8.75347 0.739583 8.59375C0.579861 8.43403 0.5 8.23611 0.5 8C0.5 7.76389 0.579861 7.56597 0.739583 7.40625C0.899306 7.24653 1.09722 7.16667 1.33333 7.16667H3.83333V6.33333C3.83333 6.09722 3.91319 5.89931 4.07292 5.73958C4.23264 5.57986 4.43056 5.5 4.66667 5.5C4.90278 5.5 5.10069 5.57986 5.26042 5.73958C5.42014 5.89931 5.5 6.09722 5.5 6.33333V9.66667C5.5 9.90278 5.42014 10.1007 5.26042 10.2604C5.10069 10.4201 4.90278 10.5 4.66667 10.5ZM8 8.83333C7.76389 8.83333 7.56597 8.75347 7.40625 8.59375C7.24653 8.43403 7.16667 8.23611 7.16667 8C7.16667 7.76389 7.24653 7.56597 7.40625 7.40625C7.56597 7.24653 7.76389 7.16667 8 7.16667H14.6667C14.9028 7.16667 15.1007 7.24653 15.2604 7.40625C15.4201 7.56597 15.5 7.76389 15.5 8C15.5 8.23611 15.4201 8.43403 15.2604 8.59375C15.1007 8.75347 14.9028 8.83333 14.6667 8.83333H8ZM11.3333 5.5C11.0972 5.5 10.8993 5.42014 10.7396 5.26042C10.5799 5.10069 10.5 4.90278 10.5 4.66667V1.33333C10.5 1.09722 10.5799 0.899306 10.7396 0.739583C10.8993 0.579861 11.0972 0.5 11.3333 0.5C11.5694 0.5 11.7674 0.579861 11.9271 0.739583C12.0868 0.899306 12.1667 1.09722 12.1667 1.33333V2.16667H14.6667C14.9028 2.16667 15.1007 2.24653 15.2604 2.40625C15.4201 2.56597 15.5 2.76389 15.5 3C15.5 3.23611 15.4201 3.43403 15.2604 3.59375C15.1007 3.75347 14.9028 3.83333 14.6667 3.83333H12.1667V4.66667C12.1667 4.90278 12.0868 5.10069 11.9271 5.26042C11.7674 5.42014 11.5694 5.5 11.3333 5.5Z"
                      fill="#09111F"
                    />
                  </Svg>
                </TouchableOpacity>
                <Modal transparent={true} visible={showModal}>
                  <View
                    style={{
                      backgroundColor: "white",
                      position: "absolute",
                      width: "100%",
                      bottom: 0,
                      height: "80%",
                      borderTopLeftRadius: "10%",
                      borderTopRightRadius: "10%",
                    }}
                  >
                    <View style={styles.modal}>
                      {/* Vigma RN:: can be replaced with <Modalbackdrop /> */}
                      <View style={styles.modalbackdrop} />
                      <View style={styles.modaldialog}>
                        <View style={styles.modalheader}>
                          <Text style={styles.filter}>{`Filter`}</Text>
                          {/* Vigma RN:: can be replaced with <Buttonclosemodal type={"filledCircle"} color={"light"} size={"small"} state={"default"} /> */}
                          <View style={styles.buttonclosemodal}>
                            {/* Vigma RN:: can be replaced with <Icon /> */}
                            <View style={styles.icon}>
                              <View style={styles.boundingbox} />
                              <TouchableOpacity onPress={()=>{setShowModal(false)}}>
                              <Svg
                                style={styles.vector}
                                width="10"
                                height="10"
                                viewBox="0 0 10 10"
                                fill="none"
                              >
                                <Path
                                  d="M5.00003 5.93337L1.73337 9.20003C1.61114 9.32226 1.45559 9.38337 1.2667 9.38337C1.07781 9.38337 0.922255 9.32226 0.800033 9.20003C0.67781 9.07781 0.616699 8.92226 0.616699 8.73337C0.616699 8.54448 0.67781 8.38892 0.800033 8.2667L4.0667 5.00003L0.800033 1.73337C0.67781 1.61114 0.616699 1.45559 0.616699 1.2667C0.616699 1.07781 0.67781 0.922255 0.800033 0.800033C0.922255 0.67781 1.07781 0.616699 1.2667 0.616699C1.45559 0.616699 1.61114 0.67781 1.73337 0.800033L5.00003 4.0667L8.2667 0.800033C8.38892 0.67781 8.54448 0.616699 8.73337 0.616699C8.92226 0.616699 9.07781 0.67781 9.20003 0.800033C9.32226 0.922255 9.38337 1.07781 9.38337 1.2667C9.38337 1.45559 9.32226 1.61114 9.20003 1.73337L5.93337 5.00003L9.20003 8.2667C9.32226 8.38892 9.38337 8.54448 9.38337 8.73337C9.38337 8.92226 9.32226 9.07781 9.20003 9.20003C9.07781 9.32226 8.92226 9.38337 8.73337 9.38337C8.54448 9.38337 8.38892 9.32226 8.2667 9.20003L5.00003 5.93337Z"
                                  fill="#09111F"
                                />
                              </Svg>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                        <View style={styles.modalbody}>
                          <View style={styles.price}>
                            {/* Vigma RN:: can be replaced with <Labelprice color={"dark"} /> */}
                            <View style={styles.labelprice}>
                              <View style={styles.left}>
                                <Text style={styles.label}>
                                  {`Price range`}
                                </Text>
                              </View>
                              <View style={styles.right} />
                            </View>
                            <View style={styles.inputwrapper}>
                              {/* Vigma RN:: can be replaced with <Inputminprice size={"medium"} state={"default"} /> */}
                              <View style={styles.inputminprice}>
                                <View style={styles.inputvaluewrapper}>
                                  <Text style={styles.inputvalue}>
                                    {`Minimum price`}
                                  </Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Divider type={"horizontal"} /> */}
                              <View style={styles.divider}>
                                <View style={styles.textwrapper}>
                                  <Text style={styles.text}>{`to`}</Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Inputmaxprice size={"medium"} state={"default"} /> */}
                              <View style={styles.inputmaxprice}>
                                <View style={styles._inputvaluewrapper}>
                                  <Text style={styles._inputvalue}>
                                    {`Maximum price`}
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </View>
                          <View style={styles.rating}>
                            {/* Vigma RN:: can be replaced with <Labelrating color={"dark"} /> */}
                            <View style={styles.labelrating}>
                              <View style={styles._left}>
                                <Text style={styles._label}>{`Rating`}</Text>
                              </View>
                              <View style={styles._right} />
                            </View>
                            <View style={styles.items}>
                              {/* Vigma RN:: can be replaced with <Star5 /> */}
                              <View style={styles.star5}>
                                {/* Vigma RN:: can be replaced with <Iconleft /> */}
                                <View style={styles.iconleft}>
                                  <View style={styles._boundingbox} />
                                  <Svg
                                    style={styles._vector}
                                    width="13"
                                    height="12"
                                    viewBox="0 0 13 12"
                                    fill="none"
                                  >
                                    <Path
                                      d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z"
                                      fill="#FAB30F"
                                    />
                                  </Svg>
                                </View>
                                <View style={styles._textwrapper}>
                                  <Text style={styles._text}>{`5`}</Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Star4 /> */}
                              <View style={styles.star4}>
                                {/* Vigma RN:: can be replaced with <_iconleft /> */}
                                <View style={styles._iconleft}>
                                  <View style={styles.__boundingbox} />
                                  <Svg
                                    style={styles.__vector}
                                    width="13"
                                    height="12"
                                    viewBox="0 0 13 12"
                                    fill="none"
                                  >
                                    <Path
                                      d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z"
                                      fill="#FAB30F"
                                    />
                                  </Svg>
                                </View>
                                <View style={styles.__textwrapper}>
                                  <Text style={styles.__text}>{`4`}</Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Star3 /> */}
                              <View style={styles.star3}>
                                {/* Vigma RN:: can be replaced with <__iconleft /> */}
                                <View style={styles.__iconleft}>
                                  <View style={styles.___boundingbox} />
                                  <Svg
                                    style={styles.___vector}
                                    width="13"
                                    height="12"
                                    viewBox="0 0 13 12"
                                    fill="none"
                                  >
                                    <Path
                                      d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z"
                                      fill="#FAB30F"
                                    />
                                  </Svg>
                                </View>
                                <View style={styles.___textwrapper}>
                                  <Text style={styles.___text}>{`3`}</Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Star2 /> */}
                              <View style={styles.star2}>
                                {/* Vigma RN:: can be replaced with <___iconleft /> */}
                                <View style={styles.___iconleft}>
                                  <View style={styles.____boundingbox} />
                                  <Svg
                                    style={styles.____vector}
                                    width="13"
                                    height="12"
                                    viewBox="0 0 13 12"
                                    fill="none"
                                  >
                                    <Path
                                      d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z"
                                      fill="#FAB30F"
                                    />
                                  </Svg>
                                </View>
                                <View style={styles.____textwrapper}>
                                  <Text style={styles.____text}>{`2`}</Text>
                                </View>
                              </View>
                              {/* Vigma RN:: can be replaced with <Star1 /> */}
                              <View style={styles.star1}>
                                {/* Vigma RN:: can be replaced with <____iconleft /> */}
                                <View style={styles.____iconleft}>
                                  <View style={styles._____boundingbox} />
                                  <Svg
                                    style={styles._____vector}
                                    width="13"
                                    height="12"
                                    viewBox="0 0 13 12"
                                    fill="none"
                                  >
                                    <Path
                                      d="M6.04 9.66508L3.11226 11.4288C2.98292 11.5111 2.8477 11.5464 2.7066 11.5346C2.56551 11.5228 2.44205 11.4758 2.33623 11.3935C2.2304 11.3112 2.1481 11.2083 2.08931 11.0849C2.03052 10.9614 2.01876 10.8232 2.05403 10.6704L2.83006 7.337L0.237421 5.0971C0.119841 4.99128 0.0463537 4.87076 0.0169587 4.73554C-0.0124364 4.60032 -0.00361785 4.46804 0.0434142 4.33871C0.0904462 4.20937 0.160994 4.10355 0.255058 4.02124C0.349122 3.93893 0.478461 3.88602 0.643073 3.86251L4.06465 3.56268L5.38743 0.423288C5.44622 0.282192 5.53735 0.17637 5.6608 0.105822C5.78426 0.035274 5.91066 0 6.04 0C6.16934 0 6.29574 0.035274 6.4192 0.105822C6.54266 0.17637 6.63378 0.282192 6.69257 0.423288L8.01535 3.56268L11.4369 3.86251C11.6015 3.88602 11.7309 3.93893 11.8249 4.02124C11.919 4.10355 11.9896 4.20937 12.0366 4.33871C12.0836 4.46804 12.0924 4.60032 12.063 4.73554C12.0336 4.87076 11.9602 4.99128 11.8426 5.0971L9.24994 7.337L10.026 10.6704C10.0612 10.8232 10.0495 10.9614 9.99069 11.0849C9.9319 11.2083 9.8496 11.3112 9.74377 11.3935C9.63795 11.4758 9.51449 11.5228 9.3734 11.5346C9.2323 11.5464 9.09708 11.5111 8.96775 11.4288L6.04 9.66508Z"
                                      fill="#FAB30F"
                                    />
                                  </Svg>
                                </View>
                                <View style={styles._____textwrapper}>
                                  <Text style={styles._____text}>{`1`}</Text>
                                </View>
                              </View>
                            </View>
                          </View>
                          <View style={styles.room}>
                            {/* Vigma RN:: can be replaced with <Labelroom color={"dark"} /> */}
                            <View style={styles.labelroom}>
                              <View style={styles.__left}>
                                <Text style={styles.__label}>{`Room`}</Text>
                              </View>
                              <View style={styles.__right} />
                            </View>
                            <View style={styles._items}>
                              <View style={styles.row}>
                                {/* Vigma RN:: can be replaced with <Bedroom /> */}
                                <View style={styles.bedroom}>
                                  <View style={styles.______textwrapper}>
                                    <Text style={styles.______text}>
                                      {`Bedroom`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Diningroom /> */}
                                <View style={styles.diningroom}>
                                  <View style={styles._______textwrapper}>
                                    <Text style={styles._______text}>
                                      {`Dining room`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Kids /> */}
                                <View style={styles.kids}>
                                  <View style={styles.________textwrapper}>
                                    <Text style={styles.________text}>
                                      {`Kids`}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                              <View style={styles._row}>
                                {/* Vigma RN:: can be replaced with <Livingroom /> */}
                                <View style={styles.livingroom}>
                                  <View style={styles._________textwrapper}>
                                    <Text style={styles._________text}>
                                      {`Living room`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Office /> */}
                                <View style={styles.office}>
                                  <View style={styles.__________textwrapper}>
                                    <Text style={styles.__________text}>
                                      {`Office`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Outdoor /> */}
                                <View style={styles.outdoor}>
                                  <View style={styles.___________textwrapper}>
                                    <Text style={styles.___________text}>
                                      {`Outdoor`}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                          <View style={styles.color}>
                            {/* Vigma RN:: can be replaced with <Labelcolor color={"dark"} /> */}
                            <View style={styles.labelcolor}>
                              <View style={styles.___left}>
                                <Text style={styles.___label}>{`Color`}</Text>
                              </View>
                              <View style={styles.___right} />
                            </View>
                            <View style={styles.__items}>
                              <View style={styles.__row}>
                                {/* Vigma RN:: can be replaced with <Black /> */}
                                <View style={styles.black}>
                                  {/* Vigma RN:: can be replaced with <_____iconleft /> */}
                                  <View style={styles._____iconleft}>
                                    <View style={styles.______boundingbox} />
                                    <Svg
                                      style={styles.______vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="black"
                                      />
                                    </Svg>
                                  </View>
                                  <View style={styles.____________textwrapper}>
                                    <Text style={styles.____________text}>
                                      {`Black`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Blue /> */}
                                <View style={styles.blue}>
                                  {/* Vigma RN:: can be replaced with <______iconleft /> */}
                                  <View style={styles.______iconleft}>
                                    <View style={styles._______boundingbox} />
                                    <Svg
                                      style={styles._______vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="#0856DB"
                                      />
                                    </Svg>
                                  </View>
                                  <View style={styles._____________textwrapper}>
                                    <Text style={styles._____________text}>
                                      {`Blue`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Gray /> */}
                                <View style={styles.gray}>
                                  {/* Vigma RN:: can be replaced with <_______iconleft /> */}
                                  <View style={styles._______iconleft}>
                                    <View style={styles.________boundingbox} />
                                    <Svg
                                      style={styles.________vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="#919BAD"
                                      />
                                    </Svg>
                                  </View>
                                  <View
                                    style={styles.______________textwrapper}
                                  >
                                    <Text style={styles.______________text}>
                                      {`Gray`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Green /> */}
                                <View style={styles.green}>
                                  {/* Vigma RN:: can be replaced with <________iconleft /> */}
                                  <View style={styles.________iconleft}>
                                    <View style={styles._________boundingbox} />
                                    <Svg
                                      style={styles._________vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="#08C754"
                                      />
                                    </Svg>
                                  </View>
                                  <View
                                    style={styles._______________textwrapper}
                                  >
                                    <Text style={styles._______________text}>
                                      {`Green`}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                              <View style={styles.___row}>
                                {/* Vigma RN:: can be replaced with <Red /> */}
                                <View style={styles.red}>
                                  {/* Vigma RN:: can be replaced with <_________iconleft /> */}
                                  <View style={styles._________iconleft}>
                                    <View
                                      style={styles.__________boundingbox}
                                    />
                                    <Svg
                                      style={styles.__________vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="#E50D24"
                                      />
                                    </Svg>
                                  </View>
                                  <View
                                    style={styles.________________textwrapper}
                                  >
                                    <Text style={styles.________________text}>
                                      {`Red`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <White /> */}
                                <View style={styles.white}>
                                  {/* Vigma RN:: can be replaced with <__________iconleft /> */}
                                  <View style={styles.__________iconleft}>
                                    <View
                                      style={styles.__________boundingbox}
                                    />
                                    <Svg
                                      style={styles.___________vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="white"
                                      />
                                    </Svg>
                                  </View>
                                  <View
                                    style={styles._________________textwrapper}
                                  >
                                    <Text style={styles._________________text}>
                                      {`White`}
                                    </Text>
                                  </View>
                                </View>
                                {/* Vigma RN:: can be replaced with <Yellow /> */}
                                <View style={styles.yellow}>
                                  {/* Vigma RN:: can be replaced with <___________iconleft /> */}
                                  <View style={styles.___________iconleft}>
                                    <View
                                      style={styles.____________boundingbox}
                                    />
                                    <Svg
                                      style={styles.____________vector}
                                      width="14"
                                      height="14"
                                      viewBox="0 0 14 14"
                                      fill="none"
                                    >
                                      <Path
                                        d="M7.00004 13.6667C6.07782 13.6667 5.21115 13.4917 4.40004 13.1417C3.58893 12.7917 2.88337 12.3167 2.28337 11.7167C1.68337 11.1167 1.20837 10.4112 0.858374 9.60004C0.508374 8.78893 0.333374 7.92226 0.333374 7.00004C0.333374 6.07782 0.508374 5.21115 0.858374 4.40004C1.20837 3.58893 1.68337 2.88337 2.28337 2.28337C2.88337 1.68337 3.58893 1.20837 4.40004 0.858374C5.21115 0.508374 6.07782 0.333374 7.00004 0.333374C7.92226 0.333374 8.78893 0.508374 9.60004 0.858374C10.4112 1.20837 11.1167 1.68337 11.7167 2.28337C12.3167 2.88337 12.7917 3.58893 13.1417 4.40004C13.4917 5.21115 13.6667 6.07782 13.6667 7.00004C13.6667 7.92226 13.4917 8.78893 13.1417 9.60004C12.7917 10.4112 12.3167 11.1167 11.7167 11.7167C11.1167 12.3167 10.4112 12.7917 9.60004 13.1417C8.78893 13.4917 7.92226 13.6667 7.00004 13.6667Z"
                                        fill="#FAB30F"
                                      />
                                    </Svg>
                                  </View>
                                  <View
                                    style={styles.__________________textwrapper}
                                  >
                                    <Text style={styles.__________________text}>
                                      {`Yellow`}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                        {/* Vigma RN:: can be replaced with <Modalfooter type={"buttons"} screen={"mobile"} /> */}
                        <View style={styles.modalfooter}>
                          {/* Vigma RN:: can be replaced with <Buttonleft type={"filled"} color={"light"} size={"medium"} state={"default"} /> */}
                          <View style={styles.buttonleft}>
                            <View style={styles.___________________textwrapper}>
                              <Text style={styles.___________________text}>
                                {`Reset`}
                              </Text>
                            </View>
                          </View>
                          {/* Vigma RN:: can be replaced with <Buttonright type={"filled"} color={"dark"} size={"medium"} state={"default"} /> */}
                          <View style={styles.buttonright}>
                            <View
                              style={styles.____________________textwrapper}
                            >
                              <Text style={styles.____________________text}>
                                {`Apply`}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </Modal>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.tabssort}>
          {/* Vigma RN:: can be replaced with <Relevance color={"dark"} size={"small"} state={"default"} /> */}
          <View style={styles.relevance}>
            <View style={styles.textwrapper}>
              <Text style={styles.text}>{`Relevance`}</Text>
            </View>
          </View>
          {/* Vigma RN:: can be replaced with <Latest  /> */}
          <View style={styles.latest}>
            <View style={styles._textwrapper}>
              <Text style={styles._text}>{`Latest`}</Text>
            </View>
          </View>
          {/* Vigma RN:: can be replaced with <Topsales  /> */}
          <View style={styles.topsales}>
            <View style={styles.__textwrapper}>
              <Text style={styles.__text}>{`Top sales`}</Text>
            </View>
          </View>
          {/* Vigma RN:: can be replaced with <Price  /> */}
          <View style={styles.price}>
            <View style={styles.___textwrapper}>
              <Text style={styles.___text}>{`Price`}</Text>
            </View>
            {/* Vigma RN:: can be replaced with <Icon  /> */}
            <View style={styles.icon}>
              <View style={styles.___boundingbox} />
              <Svg
                style={styles.___vector}
                width="6"
                height="12"
                viewBox="0 0 6 12"
                fill="none"
              >
                <Path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0.299988 3.06664C0.299988 3.26664 0.361099 3.42775 0.483321 3.54998C0.605543 3.6722 0.763877 3.73609 0.958321 3.74164C1.15277 3.7472 1.3111 3.68886 1.43332 3.56664L2.99999 1.99998L4.54999 3.54998C4.67221 3.6722 4.83332 3.73331 5.03332 3.73331C5.23332 3.73331 5.39443 3.6722 5.51665 3.54998C5.63888 3.42775 5.70277 3.26942 5.70832 3.07498C5.71388 2.88053 5.65554 2.7222 5.53332 2.59998L3.46665 0.533309C3.4111 0.466642 3.34165 0.41942 3.25832 0.391642C3.17499 0.363864 3.08888 0.349976 2.99999 0.349976C2.9111 0.349976 2.82777 0.363864 2.74999 0.391642C2.67221 0.41942 2.59999 0.466642 2.53332 0.533309L0.483321 2.58331C0.361099 2.70553 0.299988 2.86664 0.299988 3.06664ZM2.74999 11.6666C2.82777 11.6999 2.9111 11.7166 2.99999 11.7166C3.08888 11.7166 3.17499 11.6999 3.25832 11.6666C3.34165 11.6333 3.4111 11.5888 3.46665 11.5333L5.51665 9.48328C5.63888 9.36106 5.69999 9.19995 5.69999 8.99995C5.69999 8.79995 5.63888 8.63884 5.51665 8.51661C5.39443 8.39439 5.2361 8.3305 5.04165 8.32495C4.84721 8.31939 4.68888 8.37773 4.56665 8.49995L2.99999 10.0666L1.44999 8.51661C1.32777 8.39439 1.16665 8.33328 0.966655 8.33328C0.766654 8.33328 0.605543 8.39439 0.483321 8.51661C0.361099 8.63884 0.299988 8.79995 0.299988 8.99995C0.299988 9.19995 0.361099 9.36106 0.483321 9.48328L2.53332 11.5333C2.59999 11.5888 2.67221 11.6333 2.74999 11.6666Z"
                  fill="#4C5970"
                />
              </Svg>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.searchresult}>
        <View style={styles.title}>
          <Text style={styles.resultsforTable}>{`712 results for Table`}</Text>
        </View>
        <View style={styles.items}>
          {/* Vigma RN:: can be replaced with <____________card type={"product"} orientation={"vertical"} size={"small"} /> */}
          {firstSixElements.map((element) => (
            
            <CardProductDetail element={element} />
          ))}
          {/* Vigma RN:: can be replaced with <_____________card type={"product"} orientation={"vertical"} size={"small"} /> */}

          {/* Vigma RN:: can be replaced with <______________card type={"product"} orientation={"vertical"} size={"small"} /> */}
        </View>
      </View>
    </View>
  );
};
export default SearchResult;
const styles = StyleSheet.create({
  navbar: {
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 0,
  },
  navtop: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
  itemleftwrapper: {
    flexShrink: 0,
    paddingLeft: 0,
    paddingRight: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
  },
  itemleft: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineArrow_back: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 4,
    bottom: 4,
    left: 3,
    overflow: "visible",
  },
  inputwrapper: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 8,
  },
  inputfield: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12,
  },
  iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  _vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible",
  },
  inputvaluewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 2,
    paddingHorizontal: 4,
  },
  inputvalue: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
  },
  itemrightwrapper: {
    flexShrink: 0,
    paddingLeft: 4,
    paddingRight: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
  },
  itemright: {
    flexShrink: 0,
    height: 40,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineTune: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  __vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible",
  },
  tabssort: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 12,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 14,
    paddingHorizontal: 24,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  relevance: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0,
  },
  textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  latest: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0,
  },
  _textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  _text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  topsales: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0,
  },
  __textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  __text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  price: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0,
  },
  ___textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  ___text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  ___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    left: 5,
    width: 5,
    height: 11,
    overflow: "visible",
  },

  searchresult: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingVertical: 0,
    paddingHorizontal: 24,
  },
  title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
  },
  resultsforTable: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },

  items: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 24,
    flexWrap: "wrap",
  },

  ____________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
    width: "47%",
    margin: 10, // Set width to 50%
  },

  _image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },

  cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  _______________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },

  currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },

  initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },

  _badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  _label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },

  _________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ____________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  ______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },

  card: {
    flexShrink: 0,
    height: 154,
    width: 256,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },




  modal: {
    flexShrink: 0,
 
    
    paddingTop: 54,
    paddingBottom: 0,
    alignItems: "center",
    justifyContent: "flex-end",
    rowGap: 0,
    paddingHorizontal: 0
    },
    modalbackdrop: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  
    alignItems: "flex-start",
    rowGap: 0
    },
    modaldialog: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 0
    },
    modalheader: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 16,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12
    },
    filter: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24
    },
    buttonclosemodal: {
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999
    },


    modalbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 10,
    paddingBottom: 76,
    alignItems: "flex-start",
    rowGap: 24,
    paddingHorizontal: 24
    },

    labelprice: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
    },
    left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
    },

    right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
    },

    inputminprice: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
    },

    divider: {
    flexShrink: 0,
    width: 39,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0
    },

    inputmaxprice: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
    },
    _inputvaluewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 2,
    paddingHorizontal: 4
    },
    _inputvalue: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(145, 155, 173, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20
    },
    rating: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
    },
    labelrating: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
    },
    _left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
    },

    _right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
    },

    star5: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },


    star4: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },

    star3: {
      flexShrink: 0,
      backgroundColor: "rgba(240, 242, 245, 1)",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      columnGap: 0,
      padding: 6,
      borderRadius: 8
    },
    __iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },


    star2: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ___iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    ____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
   
    },
    ____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible"
    },
    ____textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ____text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    star1: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ____iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    _____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },

    _____textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _____text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    room: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
    },
    labelroom: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
    },
    __left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
    },
    __label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    __right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
    },
    _items: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
    },
    row: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 8
    },
    bedroom: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ______textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ______text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    diningroom: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _______textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _______text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    kids: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    _row: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 8
    },
    livingroom: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    office: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    __________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    __________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    outdoor: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ___________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ___________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    color: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
    },
    labelcolor: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12
    },
    ___left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4
    },
    ___label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    ___right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4
    },
    __items: {
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 8
    },
    __row: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 8
    },
    black: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _____iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    ______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },

    ____________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ____________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    blue: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ______iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    _______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },
    _______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    _____________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _____________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    gray: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _______iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    ________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },
    ________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    ______________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ______________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    green: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ________iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    _________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },
    _________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    _______________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _______________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    ___row: {
    flexShrink: 0,
    width: 312,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 8
    },
    red: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    _________iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },
    __________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    },
    __________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    ________________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ________________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    white: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    __________iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },

    ___________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    _________________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    _________________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    yellow: {
    flexShrink: 0,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    padding: 6,
    borderRadius: 8
    },
    ___________iconleft: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0
    },

    ____________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 1,
    bottom: 1,
    left: 1,
    overflow: "visible"
    },
    __________________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    __________________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16
    },
    modalfooter: {
    position: "absolute",
    flexShrink: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 12,
    paddingVertical: 12,
    paddingHorizontal: 24
    },
    buttonleft: {
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12
    },
    ___________________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ___________________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
    },
    buttonright: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    backgroundColor: "rgba(9, 17, 31, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 12
    },
    ____________________textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4
    },
    ____________________text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20
    }
});
