import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Slot, Tabs, router } from 'expo-router'
import { Provider } from 'react-redux'
import store from '../store'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { service } from '../service'

const _layout = () => {

  const [isLoading, setIsLoading] = useState(true)
  const [token, setToken] = useState()

  const verifToken = async () => {
    const value = await AsyncStorage.getItem('token');
    if (value) {
      setToken(JSON.parse(value).token)
      service.setSecurityData({
        headers: {
          authorization: "Bearer " + JSON.parse(value).token
        }
      })

    }
    setIsLoading(false)


  }
  useEffect(() => {
    console.log("init layout")
    verifToken()

  }, [])

  useEffect(() => {
   if(!isLoading){
    console.log(token)
    if(!token){
      router.push({pathname: "auth/signIn"})
    }

   }

  }, [isLoading])
  if (isLoading) {
    return null
  }
  else {
    return (
      <Provider store={store}>
        <Slot></Slot>
      </Provider>
    )
  }

}

export default _layout

const styles = StyleSheet.create({})