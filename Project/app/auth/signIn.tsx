import {
  Button,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState } from "react";
import { TextInput } from "react-native-gesture-handler";
import { enableExpoCliLogging } from "expo/build/logs/Logs";
import { service } from "../../service";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { router } from "expo-router";
import { Line, Path, Svg } from "react-native-svg";

const SignIn = () => {
  const screenWidth = Dimensions.get("window").width;
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const login = async () => {
    const { data } = await service.login.loginCreate({ email, password });
    const value = JSON.stringify(data?.tokens.access);
    try {
      service.setSecurityData({
        headers: {
          authorization: "Bearer " + data?.tokens.access.token,
        },
      });
      await AsyncStorage.setItem("token", value);

      router.push({
        pathname: "(tabs)/home",
      });
    } catch (e) {
      // saving error
    }
    console.log(data);
  };

  return (
    <View style={styles.logIn}>
      <View style={styles.logo}>
        <View style={styles._logo}>
          <Svg
            style={styles.logomark}
            width="36"
            height="24"
            viewBox="0 0 36 24"
            fill="none"
          >
            <Path
              d="M14.5167 23.9541C13.5594 24.1064 12.5804 23.8773 11.7907 23.316C11.001 22.7548 10.4639 21.9063 10.295 20.9535L7.45301 4.26077C7.30037 3.3052 7.52993 2.32803 8.09221 1.5398C8.6545 0.751572 9.50451 0.215385 10.4591 0.0467716C11.4164 -0.105591 12.3954 0.123542 13.185 0.684799C13.9747 1.24606 14.5119 2.09451 14.6808 3.04734L17.5228 19.7425C17.6747 20.6977 17.4448 21.6742 16.8826 22.4619C16.3204 23.2496 15.4708 23.7854 14.5167 23.9541Z"
              fill="#0856DB"
            />
            <Path
              d="M14.5167 23.9541C13.5594 24.1064 12.5804 23.8773 11.7907 23.316C11.001 22.7548 10.4639 21.9063 10.295 20.9535L7.45301 4.26077C7.30037 3.3052 7.52993 2.32803 8.09221 1.5398C8.6545 0.751572 9.50451 0.215385 10.4591 0.0467716C11.4164 -0.105591 12.3954 0.123542 13.185 0.684799C13.9747 1.24606 14.5119 2.09451 14.6808 3.04734L17.5228 19.7425C17.6747 20.6977 17.4448 21.6742 16.8826 22.4619C16.3204 23.2496 15.4708 23.7854 14.5167 23.9541Z"
              fill="black"
              fillOpacity="0.2"
            />
            <Path
              d="M24.7055 23.9539C23.7482 24.1063 22.7692 23.8772 21.9796 23.3159C21.1899 22.7547 20.6527 21.9062 20.4838 20.9534L17.6424 4.26063C17.5584 3.78954 17.5685 3.3065 17.6721 2.8393C17.7757 2.3721 17.9708 1.92995 18.2462 1.53828C18.5216 1.14661 18.8718 0.813138 19.2768 0.557045C19.6817 0.300952 20.1334 0.127288 20.6058 0.0460392C21.5631 -0.106323 22.5421 0.122809 23.3318 0.684066C24.1215 1.24532 24.6586 2.09377 24.8276 3.0466L27.6695 19.7417C27.7532 20.2127 27.7429 20.6955 27.639 21.1625C27.5352 21.6295 27.34 22.0713 27.0646 22.4627C26.7891 22.8542 26.439 23.1874 26.0341 23.4433C25.6293 23.6992 25.1778 23.8727 24.7055 23.9539Z"
              fill="#0856DB"
            />
            <Path
              d="M24.7055 23.9539C23.7482 24.1063 22.7692 23.8772 21.9796 23.3159C21.1899 22.7547 20.6527 21.9062 20.4838 20.9534L17.6424 4.26063C17.5584 3.78954 17.5685 3.3065 17.6721 2.8393C17.7757 2.3721 17.9708 1.92995 18.2462 1.53828C18.5216 1.14661 18.8718 0.813138 19.2768 0.557045C19.6817 0.300952 20.1334 0.127288 20.6058 0.0460392C21.5631 -0.106323 22.5421 0.122809 23.3318 0.684066C24.1215 1.24532 24.6586 2.09377 24.8276 3.0466L27.6695 19.7417C27.7532 20.2127 27.7429 20.6955 27.639 21.1625C27.5352 21.6295 27.34 22.0713 27.0646 22.4627C26.7891 22.8542 26.439 23.1874 26.0341 23.4433C25.6293 23.6992 25.1778 23.8727 24.7055 23.9539Z"
              fill="black"
              fillOpacity="0.2"
            />
            <Path
              d="M2.2569 23.6702C1.37178 23.2743 0.679039 22.5457 0.329338 21.6427C-0.0203619 20.7397 0.0012694 19.7354 0.389527 18.8483L7.73697 2.19393C8.13352 1.31029 8.86361 0.618697 9.7684 0.269625C10.6732 -0.0794456 11.6795 -0.0577547 12.5683 0.329979C13.4534 0.725803 14.1462 1.45445 14.4959 2.35745C14.8456 3.26044 14.824 4.26473 14.4357 5.15189L7.04677 21.8062C6.85728 22.2445 6.5825 22.6409 6.23836 22.9723C5.89423 23.3038 5.48758 23.5638 5.04201 23.7372C4.59644 23.9106 4.12082 23.9939 3.64272 23.9824C3.16463 23.9709 2.69358 23.8648 2.2569 23.6702Z"
              fill="#0856DB"
            />
            <Path
              d="M12.406 23.6702C11.5209 23.2743 10.8281 22.5457 10.4784 21.6427C10.1287 20.7397 10.1503 19.7354 10.5386 18.8483L17.886 2.19393C18.2826 1.31029 19.0127 0.618696 19.9175 0.269625C20.8223 -0.0794455 21.8286 -0.0577547 22.7174 0.329979C23.6025 0.725803 24.2953 1.45445 24.645 2.35745C24.9947 3.26044 24.973 4.26473 24.5848 5.15189L17.2367 21.8062C16.8419 22.6912 16.1121 23.3842 15.2068 23.7335C14.3015 24.0828 13.2945 24.06 12.406 23.6702Z"
              fill="#0856DB"
            />
            <Path
              d="M22.5551 23.67C21.67 23.2742 20.9773 22.5455 20.6276 21.6425C20.2779 20.7395 20.2995 19.7353 20.6878 18.8481L28.0761 2.19377C28.4726 1.31028 29.2026 0.618801 30.1073 0.269742C31.0119 -0.0793177 32.0181 -0.057726 32.9069 0.32982C33.792 0.725644 34.4847 1.45429 34.8344 2.35729C35.1841 3.26028 35.1625 4.26457 34.7742 5.15173L27.3859 21.8061C27.1931 22.2465 26.9148 22.6445 26.5671 22.977C26.2193 23.3096 25.809 23.57 25.3598 23.7433C24.9107 23.9166 24.4315 23.9994 23.9501 23.9868C23.4687 23.9742 22.9946 23.8665 22.5551 23.67Z"
              fill="#0856DB"
            />
          </Svg>
          <Svg
            style={styles.mAYNOOTH}
            width="115"
            height="16"
            viewBox="0 0 115 16"
            fill="none"
          >
            <Path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M2.50185 15.49H0.0818481V0.529989H2.41962L7.23185 12.2803L12.0441 0.529989H14.4219V15.49H12.0018V7.77999C12.0018 7.2979 12.0085 6.8812 12.022 6.53038C12.0247 6.46216 12.0273 6.39547 12.0299 6.33031L8.32033 15.49H6.14332L2.46049 6.38816C2.46093 6.40963 2.46136 6.43126 2.46176 6.45305C2.48852 7.17663 2.50185 8.04578 2.50185 9.05999V15.49ZM2.11185 4.85999C2.14473 5.0737 2.17 5.35834 2.18768 5.71392C2.19868 5.93515 2.20673 6.18384 2.21185 6.45999C2.23851 7.17999 2.25185 8.04666 2.25185 9.05999V15.24H0.331848V0.779989H2.25185L7.23185 12.94L12.2118 0.779989H14.1719V15.24H12.2518V7.77999C12.2518 7.29999 12.2585 6.88666 12.2718 6.53999C12.2849 6.2003 12.298 5.89901 12.311 5.63613C12.3113 5.63074 12.3116 5.62535 12.3118 5.61999C12.3252 5.33999 12.3452 5.07332 12.3719 4.81999L8.15185 15.24H6.31185L2.11185 4.85999ZM19.1153 15.49H16.5414L22.004 0.529989H24.5335L30.0168 15.49H27.4234L26.1234 11.89H20.3953L19.1153 15.49ZM20.2189 11.64H23.2589H26.2989L27.5989 15.24H29.6589L24.3589 0.779989H22.1789L16.8989 15.24H18.9389L20.2189 11.64ZM23.2538 3.98555L21.176 9.68999H25.3606L23.2605 3.97614L23.2591 3.9709C23.2573 3.97574 23.2555 3.98071 23.2538 3.98555ZM23.13 3.56208C23.132 3.55505 23.1341 3.54803 23.1361 3.541C23.1368 3.53851 23.1375 3.53602 23.1383 3.53353C23.1453 3.50902 23.1522 3.4845 23.1589 3.45999L23.2589 3.05999C23.2722 3.16666 23.3055 3.29999 23.3589 3.45999C23.3655 3.47829 23.3721 3.4966 23.3785 3.5149L23.3805 3.52043C23.3844 3.53169 23.3884 3.54296 23.3922 3.55423C23.4319 3.66948 23.4674 3.78474 23.4989 3.89999L25.7189 9.93999H20.8189L23.0189 3.89999C23.0598 3.78736 23.0969 3.67471 23.13 3.56208ZM36.3353 9.35999V15.24H34.3753V9.35999L29.4753 0.779989H31.6753L34.8953 6.55999C34.9886 6.73332 35.0686 6.89332 35.1353 7.03999C35.1699 7.10341 35.2045 7.16933 35.2391 7.23774C35.2845 7.32753 35.3299 7.42161 35.3753 7.51999C35.44 7.40347 35.4859 7.30581 35.5129 7.227C35.5137 7.22465 35.5145 7.22231 35.5153 7.21999L35.6353 6.93999C35.6886 6.84666 35.762 6.71999 35.8553 6.55999L39.0553 0.779989H41.1953L36.3353 9.35999ZM35.6379 6.43646L38.9079 0.529989H41.6242L36.5853 9.42588V15.49H34.1253V9.42635L29.0446 0.529989H31.8222L35.1154 6.44145C35.2091 6.6155 35.2905 6.77783 35.3591 6.92825C35.3608 6.93141 35.3625 6.93458 35.3643 6.93775L35.4111 6.82837L35.4182 6.81596C35.4718 6.72216 35.5451 6.59557 35.6379 6.43646ZM46.0942 15.49H43.6342V0.529989H45.9786L52.9942 11.133V0.529989H55.4542V15.49H53.1098L46.0942 4.887V15.49ZM45.8442 4.05611V15.24H43.8842V0.779989H45.8442L53.2442 11.9639V0.779989H55.2042V15.24H53.2442L45.8442 4.05611ZM69.1807 14.7541C68.0969 15.4072 66.8444 15.73 65.4317 15.73C64.0446 15.73 62.7996 15.4067 61.7039 14.7548L61.7007 14.7529C60.621 14.0885 59.777 13.1744 59.1702 12.016C58.5619 10.8546 58.2617 9.5135 58.2617 7.99999C58.2617 6.49941 58.562 5.16512 59.1702 4.00399L59.1713 4.00193C59.7913 2.84462 60.6415 1.9312 61.7207 1.26708C62.8044 0.600138 64.0508 0.269989 65.4517 0.269989C66.8654 0.269989 68.1186 0.599902 69.2027 1.26708C70.2822 1.93137 71.126 2.84521 71.7328 4.00323C72.3546 5.16449 72.6617 6.49909 72.6617 7.99999C72.6617 9.50128 72.3544 10.8423 71.7329 12.0164C71.1258 13.1754 70.2738 14.09 69.1807 14.7541ZM71.5117 4.11999C70.925 2.99999 70.1117 2.11999 69.0717 1.47999C68.0317 0.83999 66.825 0.519989 65.4517 0.519989C64.0917 0.519989 62.8917 0.83999 61.8517 1.47999C60.8117 2.11999 59.9917 2.99999 59.3917 4.11999C58.805 5.23999 58.5117 6.53332 58.5117 7.99999C58.5117 9.47999 58.805 10.78 59.3917 11.9C59.9784 13.02 60.7917 13.9 61.8317 14.54C62.885 15.1667 64.085 15.48 65.4317 15.48C66.805 15.48 68.0117 15.1667 69.0517 14.54C70.105 13.9 70.925 13.02 71.5117 11.9C72.1117 10.7667 72.4117 9.46666 72.4117 7.99999C72.4117 6.53332 72.1117 5.23999 71.5117 4.11999ZM69.5074 5.17037L69.506 5.16748C69.1249 4.36728 68.5895 3.7587 67.9002 3.33264L67.8975 3.33092C67.2095 2.89309 66.3974 2.66999 65.4517 2.66999C64.52 2.66999 63.7145 2.89274 63.0259 3.33091L63.0232 3.33266C62.3336 3.75892 61.7911 4.36803 61.3967 5.16897C61.0161 5.96892 60.8217 6.91039 60.8217 7.99999C60.8217 9.08959 61.0161 10.0311 61.3967 10.831C61.7914 11.6325 62.3347 12.2492 63.0259 12.6891C63.7145 13.1272 64.52 13.35 65.4517 13.35C66.3967 13.35 67.2083 13.1272 67.8959 12.6901C68.588 12.2368 69.1247 11.6133 69.506 10.8125L69.5074 10.8096C69.901 10.0097 70.1017 9.07531 70.1017 7.99999C70.1017 6.91083 69.9008 5.96992 69.5074 5.17037ZM68.0317 12.9C68.765 12.42 69.3317 11.76 69.7317 10.92C70.145 10.08 70.3517 9.10666 70.3517 7.99999C70.3517 6.87999 70.145 5.89999 69.7317 5.05999C69.3317 4.21999 68.765 3.57332 68.0317 3.11999C67.2984 2.65332 66.4384 2.41999 65.4517 2.41999C64.4784 2.41999 63.625 2.65332 62.8917 3.11999C62.1584 3.57332 61.585 4.21999 61.1717 5.05999C60.7717 5.89999 60.5717 6.87999 60.5717 7.99999C60.5717 9.11999 60.7717 10.1 61.1717 10.94C61.585 11.78 62.1584 12.4333 62.8917 12.9C63.625 13.3667 64.4784 13.6 65.4517 13.6C66.4384 13.6 67.2984 13.3667 68.0317 12.9ZM85.5276 14.7541C84.4437 15.4072 83.1913 15.73 81.7786 15.73C80.3915 15.73 79.1465 15.4067 78.0508 14.7548L78.0475 14.7529C76.9678 14.0885 76.1239 13.1744 75.5171 12.016C74.9087 10.8546 74.6086 9.5135 74.6086 7.99999C74.6086 6.49941 74.9089 5.16512 75.5171 4.00399L75.5182 4.00193C76.1382 2.84462 76.9883 1.9312 78.0676 1.26708C79.1513 0.600138 80.3977 0.269989 81.7986 0.269989C83.2123 0.269989 84.4654 0.599902 85.5496 1.26708C86.629 1.93132 87.4728 2.84507 88.0795 4.00296C88.7014 5.16428 89.0086 6.49897 89.0086 7.99999C89.0086 9.50128 88.7013 10.8423 88.0798 12.0164C87.4726 13.1754 86.6207 14.09 85.5276 14.7541ZM85.8543 5.17037L85.8528 5.16748C85.4718 4.36728 84.9363 3.7587 84.2471 3.33264L84.2443 3.33092C83.5564 2.89309 82.7443 2.66999 81.7986 2.66999C80.8669 2.66999 80.0613 2.89275 79.3728 3.33091L79.37 3.33266C78.6804 3.75896 78.1379 4.36815 77.7435 5.1692C77.3629 5.9691 77.1686 6.91049 77.1686 7.99999C77.1686 9.08949 77.3629 10.0309 77.7435 10.8308C78.1381 11.6324 78.6815 12.2491 79.3728 12.6891C80.0613 13.1272 80.8669 13.35 81.7986 13.35C82.7437 13.35 83.5554 13.1272 84.2431 12.6899C84.935 12.2366 85.4716 11.6132 85.8529 10.8125L85.8542 10.8096C86.2479 10.0097 86.4486 9.07531 86.4486 7.99999C86.4486 6.91083 86.2477 5.96993 85.8543 5.17037ZM84.3786 12.9C83.6452 13.3667 82.7852 13.6 81.7986 13.6C80.8252 13.6 79.9719 13.3667 79.2386 12.9C78.5052 12.4333 77.9319 11.78 77.5186 10.94C77.1186 10.1 76.9186 9.11999 76.9186 7.99999C76.9186 6.87999 77.1186 5.89999 77.5186 5.05999C77.9319 4.21999 78.5052 3.57332 79.2386 3.11999C79.9719 2.65332 80.8252 2.41999 81.7986 2.41999C82.7852 2.41999 83.6452 2.65332 84.3786 3.11999C85.1119 3.57332 85.6786 4.21999 86.0786 5.05999C86.4919 5.89999 86.6986 6.87999 86.6986 7.99999C86.6986 9.10666 86.4919 10.08 86.0786 10.92C85.6786 11.76 85.1119 12.42 84.3786 12.9ZM96.4776 2.57999V15.24H94.5176V2.57999H90.1576V0.779989H100.818V2.57999H96.4776ZM94.2676 2.82999H89.9076V0.529989H101.068V2.82999H96.7276V15.49H94.2676V2.82999ZM105.798 15.49H103.338V0.529989H105.798V6.74999H112.458V0.529989H114.918V15.49H112.458V9.06999H105.798V15.49ZM112.708 8.81999V15.24H114.668V0.779989H112.708V6.99999H105.548V0.779989H103.588V15.24H105.548V8.81999H112.708ZM87.8586 11.9C88.4586 10.7667 88.7586 9.46666 88.7586 7.99999C88.7586 6.53332 88.4586 5.23999 87.8586 4.11999C87.2719 2.99999 86.4586 2.11999 85.4186 1.47999C84.3786 0.83999 83.1719 0.519989 81.7986 0.519989C80.4386 0.519989 79.2386 0.83999 78.1986 1.47999C77.1586 2.11999 76.3386 2.99999 75.7386 4.11999C75.1519 5.23999 74.8586 6.53332 74.8586 7.99999C74.8586 9.47999 75.1519 10.78 75.7386 11.9C76.3252 13.02 77.1386 13.9 78.1786 14.54C79.2319 15.1667 80.4319 15.48 81.7786 15.48C83.1519 15.48 84.3586 15.1667 85.3986 14.54C86.4519 13.9 87.2719 13.02 87.8586 11.9Z"
              fill="#09111F"
            />
            <Path
              d="M2.11185 4.85999C2.14473 5.0737 2.17 5.35834 2.18768 5.71392C2.19868 5.93515 2.20673 6.18384 2.21185 6.45999C2.23851 7.17999 2.25185 8.04666 2.25185 9.05999V15.24H0.331848V0.779989H2.25185L7.23185 12.94L12.2118 0.779989H14.1719V15.24H12.2518V7.77999C12.2518 7.29999 12.2585 6.88666 12.2718 6.53999C12.2849 6.2003 12.298 5.89901 12.311 5.63613L12.3118 5.61999C12.3252 5.33999 12.3452 5.07332 12.3719 4.81999L8.15185 15.24H6.31185L2.11185 4.85999Z"
              fill="#09111F"
            />
            <Path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M27.5989 15.24L26.2989 11.64H23.2589H20.2189L18.9389 15.24H16.8989L22.1789 0.779989H24.3589L29.6589 15.24H27.5989ZM23.13 3.56208L23.1361 3.541L23.1383 3.53353C23.1453 3.50902 23.1522 3.4845 23.1589 3.45999L23.2589 3.05999C23.2722 3.16666 23.3055 3.29999 23.3589 3.45999L23.3785 3.5149L23.3805 3.52043L23.3922 3.55423C23.4319 3.66948 23.4674 3.78474 23.4989 3.89999L25.7189 9.93999H20.8189L23.0189 3.89999C23.0598 3.78736 23.0969 3.67471 23.13 3.56208Z"
              fill="#09111F"
            />
            <Path
              d="M34.8953 6.55999L31.6753 0.779989H29.4753L34.3753 9.35999V15.24H36.3353V9.35999L41.1953 0.779989H39.0553L35.8553 6.55999C35.762 6.71999 35.6886 6.84666 35.6353 6.93999L35.5153 7.21999L35.5129 7.227C35.4859 7.30581 35.44 7.40347 35.3753 7.51999C35.3299 7.42161 35.2845 7.32753 35.2391 7.23774C35.2045 7.16933 35.1699 7.10341 35.1353 7.03999C35.0686 6.89332 34.9886 6.73332 34.8953 6.55999Z"
              fill="#09111F"
            />
            <Path
              d="M53.2442 15.24L45.8442 4.05611V15.24H43.8842V0.779989H45.8442L53.2442 11.9639V0.779989H55.2042V15.24H53.2442Z"
              fill="#09111F"
            />
            <Path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M71.5117 4.11999C70.925 2.99999 70.1117 2.11999 69.0717 1.47999C68.0317 0.83999 66.825 0.519989 65.4517 0.519989C64.0917 0.519989 62.8917 0.83999 61.8517 1.47999C60.8117 2.11999 59.9917 2.99999 59.3917 4.11999C58.805 5.23999 58.5117 6.53332 58.5117 7.99999C58.5117 9.47999 58.805 10.78 59.3917 11.9C59.9784 13.02 60.7917 13.9 61.8317 14.54C62.885 15.1667 64.085 15.48 65.4317 15.48C66.805 15.48 68.0117 15.1667 69.0517 14.54C70.105 13.9 70.925 13.02 71.5117 11.9C72.1117 10.7667 72.4117 9.46666 72.4117 7.99999C72.4117 6.53332 72.1117 5.23999 71.5117 4.11999ZM68.0317 12.9C68.765 12.42 69.3317 11.76 69.7317 10.92C70.145 10.08 70.3517 9.10666 70.3517 7.99999C70.3517 6.87999 70.145 5.89999 69.7317 5.05999C69.3317 4.21999 68.765 3.57332 68.0317 3.11999C67.2984 2.65332 66.4384 2.41999 65.4517 2.41999C64.4784 2.41999 63.625 2.65332 62.8917 3.11999C62.1584 3.57332 61.585 4.21999 61.1717 5.05999C60.7717 5.89999 60.5717 6.87999 60.5717 7.99999C60.5717 9.11999 60.7717 10.1 61.1717 10.94C61.585 11.78 62.1584 12.4333 62.8917 12.9C63.625 13.3667 64.4784 13.6 65.4517 13.6C66.4384 13.6 67.2984 13.3667 68.0317 12.9Z"
              fill="#09111F"
            />
            <Path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M88.7586 7.99999C88.7586 9.46666 88.4586 10.7667 87.8586 11.9C87.2719 13.02 86.4519 13.9 85.3986 14.54C84.3586 15.1667 83.1519 15.48 81.7786 15.48C80.4319 15.48 79.2319 15.1667 78.1786 14.54C77.1386 13.9 76.3252 13.02 75.7386 11.9C75.1519 10.78 74.8586 9.47999 74.8586 7.99999C74.8586 6.53332 75.1519 5.23999 75.7386 4.11999C76.3386 2.99999 77.1586 2.11999 78.1986 1.47999C79.2386 0.83999 80.4386 0.519989 81.7986 0.519989C83.1719 0.519989 84.3786 0.83999 85.4186 1.47999C86.4586 2.11999 87.2719 2.99999 87.8586 4.11999C88.4586 5.23999 88.7586 6.53332 88.7586 7.99999ZM84.3786 12.9C83.6452 13.3667 82.7852 13.6 81.7986 13.6C80.8252 13.6 79.9719 13.3667 79.2386 12.9C78.5052 12.4333 77.9319 11.78 77.5186 10.94C77.1186 10.1 76.9186 9.11999 76.9186 7.99999C76.9186 6.87999 77.1186 5.89999 77.5186 5.05999C77.9319 4.21999 78.5052 3.57332 79.2386 3.11999C79.9719 2.65332 80.8252 2.41999 81.7986 2.41999C82.7852 2.41999 83.6452 2.65332 84.3786 3.11999C85.1119 3.57332 85.6786 4.21999 86.0786 5.05999C86.4919 5.89999 86.6986 6.87999 86.6986 7.99999C86.6986 9.10666 86.4919 10.08 86.0786 10.92C85.6786 11.76 85.1119 12.42 84.3786 12.9Z"
              fill="#09111F"
            />
            <Path
              d="M100.818 2.57999H96.4776V15.24H94.5176V2.57999H90.1576V0.779989H100.818V2.57999Z"
              fill="#09111F"
            />
            <Path
              d="M112.708 8.81999H105.548V15.24H103.588V0.779989H105.548V6.99999H112.708V0.779989H114.668V15.24H112.708V8.81999Z"
              fill="#09111F"
            />
          </Svg>
        </View>
      </View>

      <View style={styles.formlogin}>
        {/* Vigma RN:: can be replaced with <Inputemail /> */}
        <View style={styles.inputemail}>
          {/* Vigma RN:: can be replaced with <Labeltop color={"dark"} /> */}
          <View style={styles.labeltop}>
            <View style={styles.left}>
              <Text style={styles.label}>{`UserName`}</Text>
            </View>
            <View style={styles.right} />
          </View>
          {/* Vigma RN:: can be replaced with <Inputfield size={"medium"} state={"default"} /> */}

          <View style={styles.inputfield}>
            <View style={styles.iconleft}>
              <Svg
                style={styles.vector}
                width="18"
                height="14"
                viewBox="0 0 101 101"
                id="user"
                fill="none"
              >
                <Path
                  d="M50.4 54.5c10.1 0 18.2-8.2 18.2-18.2S60.5 18 50.4 18s-18.2 8.2-18.2 18.2 8.1 18.3 18.2 18.3zm0-31.7c7.4 0 13.4 6 13.4 13.4s-6 13.4-13.4 13.4S37 43.7 37 36.3s6-13.5 13.4-13.5zM18.8 83h63.4c1.3 0 2.4-1.1 2.4-2.4 0-12.6-10.3-22.9-22.9-22.9H39.3c-12.6 0-22.9 10.3-22.9 22.9 0 1.3 1.1 2.4 2.4 2.4zm20.5-20.5h22.4c9.2 0 16.7 6.8 17.9 15.7H21.4c1.2-8.9 8.7-15.7 17.9-15.7z"
                  fill="#919BAD"
                />
              </Svg>
            </View>
            <View style={styles.inputvaluewrapper}>
              <TextInput
                style={styles.inputvalue}
                value={email}
                onChangeText={(text) => setEmail(text)}
                placeholder="Enter usernme"
              />
            </View>
          </View>
        </View>
        {/* Vigma RN:: can be replaced with <Inputpassword /> */}
        <View style={styles.inputpassword}>
          {/* Vigma RN:: can be replaced with <_labeltop color={"dark"} /> */}
          <View style={styles._labeltop}>
            <View style={styles._left}>
              <Text style={styles._label}>{`Password`}</Text>
            </View>
            <View style={styles._right} />
          </View>
          {/* Vigma RN:: can be replaced with <_inputfield size={"medium"} state={"default"} /> */}
          <View style={styles._inputfield}>
            {/* Vigma RN:: can be replaced with <_iconleft /> */}
            <View style={styles._iconleft}>
              <View style={styles._boundingbox} />
              <Svg
                style={styles._vector}
                width="14"
                height="19"
                viewBox="0 0 14 19"
                fill="none"
              >
                <Path
                  d="M1.99999 18.3333C1.54166 18.3333 1.1493 18.1701 0.822912 17.8437C0.496523 17.5174 0.333328 17.125 0.333328 16.6667V8.33333C0.333328 7.875 0.496523 7.48264 0.822912 7.15625C1.1493 6.82986 1.54166 6.66667 1.99999 6.66667H2.83333V5C2.83333 3.84722 3.23958 2.86458 4.05208 2.05208C4.86458 1.23958 5.84722 0.833332 6.99999 0.833332C8.15277 0.833332 9.13541 1.23958 9.94791 2.05208C10.7604 2.86458 11.1667 3.84722 11.1667 5V6.66667H12C12.4583 6.66667 12.8507 6.82986 13.1771 7.15625C13.5035 7.48264 13.6667 7.875 13.6667 8.33333V16.6667C13.6667 17.125 13.5035 17.5174 13.1771 17.8437C12.8507 18.1701 12.4583 18.3333 12 18.3333H1.99999ZM1.99999 16.6667H12V8.33333H1.99999V16.6667ZM6.99999 14.1667C7.45833 14.1667 7.85069 14.0035 8.17708 13.6771C8.50347 13.3507 8.66666 12.9583 8.66666 12.5C8.66666 12.0417 8.50347 11.6493 8.17708 11.3229C7.85069 10.9965 7.45833 10.8333 6.99999 10.8333C6.54166 10.8333 6.1493 10.9965 5.82291 11.3229C5.49652 11.6493 5.33333 12.0417 5.33333 12.5C5.33333 12.9583 5.49652 13.3507 5.82291 13.6771C6.1493 14.0035 6.54166 14.1667 6.99999 14.1667ZM4.49999 6.66667H9.49999V5C9.49999 4.30555 9.25694 3.71528 8.77083 3.22917C8.28472 2.74305 7.69444 2.5 6.99999 2.5C6.30555 2.5 5.71527 2.74305 5.22916 3.22917C4.74305 3.71528 4.49999 4.30555 4.49999 5V6.66667Z"
                  fill="#919BAD"
                />
              </Svg>
            </View>
            <View style={styles._inputvaluewrapper}>
              <TextInput
                style={styles._inputvalue}
                value={password}
                secureTextEntry={true}
                onChangeText={(text) => setPassword(text)}
              ></TextInput>
            </View>
          </View>
        </View>
        <View style={styles.remembermewrapper}>
          {/* Vigma RN:: can be replaced with <Checkboxrememberme size={"medium"} state={"uncheck"} /> */}
          <View style={styles.checkboxrememberme}>
            {/* Vigma RN:: can be replaced with <Icon /> */}
            <View style={styles.icon}>
              <View style={styles.__boundingbox} />
              <Svg
                style={styles.__vector}
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
              >
                <Path
                  d="M2.16667 15.5C1.70833 15.5 1.31597 15.3368 0.989583 15.0104C0.663194 14.684 0.5 14.2917 0.5 13.8333V2.16667C0.5 1.70833 0.663194 1.31597 0.989583 0.989583C1.31597 0.663194 1.70833 0.5 2.16667 0.5H13.8333C14.2917 0.5 14.684 0.663194 15.0104 0.989583C15.3368 1.31597 15.5 1.70833 15.5 2.16667V13.8333C15.5 14.2917 15.3368 14.684 15.0104 15.0104C14.684 15.3368 14.2917 15.5 13.8333 15.5H2.16667ZM2.16667 13.8333H13.8333V2.16667H2.16667V13.8333Z"
                  fill="#09111F"
                />
              </Svg>
            </View>
            <Text style={styles.__label}>{`Remember me`}</Text>
          </View>
          <Text style={styles.linkforgotpassword}>{`Forgot password?`}</Text>
        </View>
        {/* Vigma RN:: can be replaced with <Buttonlogin type={"filled"} color={"dark"} size={"medium"} state={"default"} /> */}
        <View style={styles.buttonlogin}>
          <TouchableOpacity onPress={() => login()} style={styles.textwrapper}>
            <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.divider}>
        <View style={styles.linewrapper}>
          <Svg
            style={[styles.line,{width:"100%"}]}
            
            height={1}
            viewBox={`0 0 ${screenWidth} 1`}
            fill="none"
          >
            <Line y1="0.5" x2={screenWidth} y2="0.5" stroke="#E1E5EB" />
          </Svg>
        </View>
        <View style={styles.textwrapperD}>
          <Text style={styles.textD}>{`or continue with`}</Text>
        </View>
        <View style={styles._linewrapper}>
          <Svg
            style={[styles._line,{width:"100%"}]}
           
            height="1"
            viewBox={`0 0 ${screenWidth} 1`}
            fill="none"
          >
            <Line x1="0.5" y1="0.5" x2={screenWidth} y2="0.5" stroke="#E1E5EB" />
          </Svg>
        </View>
      </View>
      <View style={styles.buttonsocialwrapper}>
        {/* Vigma RN:: can be replaced with <Buttongoogle type={"outline"} color={"dark"} size={"medium"} state={"default"} /> */}
        <View style={styles.buttongoogle}>
          {/* Vigma RN:: can be replaced with <Iconleft /> */}
          <View style={styles.iconleftG}>
            <Svg
              style={styles.vectorG}
              width="9"
              height="9"
              viewBox="0 0 9 9"
              fill="none"
            >
              <Path
                d="M8.8 2.20833C8.8 1.55833 8.74167 0.933328 8.63334 0.333328H0V3.87916H4.93333C4.72083 5.025 4.075 5.99583 3.10417 6.64583V8.94583H6.06667C7.8 7.35 8.8 5 8.8 2.20833Z"
                fill="#4285F4"
              />
            </Svg>
            <Svg
              style={styles._vectorG}
              width="16"
              height="9"
              viewBox="0 0 16 9"
              fill="none"
            >
              <Path
                d="M8.99999 8.16667C11.475 8.16667 13.55 7.34583 15.0667 5.94583L12.1042 3.64584C11.2833 4.19584 10.2333 4.52083 8.99999 4.52083C6.61249 4.52083 4.59165 2.90833 3.87082 0.741669H0.808319V3.11667C2.31665 6.1125 5.41665 8.16667 8.99999 8.16667Z"
                fill="#34A853"
              />
            </Svg>
            <Svg
              style={styles.__vectorG}
              width="5"
              height="10"
              viewBox="0 0 5 10"
              fill="none"
            >
              <Path
                d="M4.87084 6.74166C4.68751 6.19167 4.58334 5.60417 4.58334 5C4.58334 4.39583 4.68751 3.80833 4.87084 3.25833V0.883331H1.80834C1.16668 2.16071 0.832778 3.57051 0.833344 5C0.833344 6.47917 1.18751 7.87917 1.80834 9.11667L4.87084 6.74166Z"
                fill="#FBBC05"
              />
            </Svg>
            <Svg
              style={styles.___vectorG}
              width="16"
              height="9"
              viewBox="0 0 16 9"
              fill="none"
            >
              <Path
                d="M8.99999 4.47916C10.3458 4.47916 11.5542 4.94166 12.5042 5.84999L15.1333 3.22083C13.5458 1.74166 11.4708 0.833328 8.99999 0.833328C5.41665 0.833328 2.31665 2.88749 0.808319 5.88333L3.87082 8.25833C4.59165 6.09166 6.61249 4.47916 8.99999 4.47916Z"
                fill="#EA4335"
              />
            </Svg>
          </View>
          <View style={styles.textwrapperG}>
            <Text style={styles.textG}>{`Google`}</Text>
          </View>
        </View>
        {/* Vigma RN:: can be replaced with <Buttonfacebook type={"outline"} color={"dark"} size={"medium"} state={"default"} /> */}
        <View style={styles.buttongoogle}>
          {/* Vigma RN:: can be replaced with <Iconleft /> */}
          <View style={styles.iconleftG}>
            <Svg width="20" height="20" viewBox="0 0 20 20" fill="none">
              <Path
                d="M20 9.99999C20 4.47714 15.5229 -7.62939e-06 10 -7.62939e-06C4.47715 -7.62939e-06 0 4.47714 0 9.99999C0 14.9913 3.65686 19.1283 8.4375 19.8785V12.8906H5.89844V9.99999H8.4375V7.79687C8.4375 5.29062 9.93043 3.90624 12.2146 3.90624C13.3087 3.90624 14.4531 4.10155 14.4531 4.10155V6.56249H13.1921C11.9499 6.56249 11.5625 7.33333 11.5625 8.12415V9.99999H14.3359L13.8926 12.8906H11.5625V19.8785C16.3431 19.1283 20 14.9913 20 9.99999Z"
                fill="#1877F2"
              />
              <Path
                d="M13.8926 12.8906L14.3359 10H11.5625V8.12416C11.5625 7.33334 11.9499 6.5625 13.1921 6.5625H14.4531V4.10156C14.4531 4.10156 13.3087 3.90625 12.2146 3.90625C9.93043 3.90625 8.4375 5.29062 8.4375 7.79688V10H5.89844V12.8906H8.4375V19.8785C8.94662 19.9584 9.46844 20 10 20C10.5316 20 11.0534 19.9584 11.5625 19.8785V12.8906H13.8926Z"
                fill="white"
              />
            </Svg>
          </View>
          <View style={styles.textwrapperG}>
            <Text style={styles.textG}>{`Facebook`}</Text>
          </View>
        </View>
      </View>
      <View style={styles.ctasignupwrapper}>
        <Text style={styles.donthaveanaccount}>{`Don’t have an account?`}</Text>
        <Text style={styles.linksignup}>{`Sign up`}</Text>
      </View>
    </View>
  );
};

export default SignIn;

const styles = StyleSheet.create({
  logIn: {
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "center",
    justifyContent: "center",
    rowGap: 24,
    paddingVertical: 88,
    paddingHorizontal: 0,
  },
  logo: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 24,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 6,
    paddingHorizontal: 0,
  },
  _logo: {
    flexShrink: 0,
    height: 24,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 6,
  },
  logomark: {
    flexShrink: 0,
    height: 24,
    width: 35,
  },
  mAYNOOTH: {
    flexShrink: 0,
    width: 115,
    height: 15,
    overflow: "visible",
  },

  formlogin: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "center",
    justifyContent: "center",
    rowGap: 24,
    paddingVertical: 0,
    paddingHorizontal: 24,
  },
  inputemail: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 4,
  },
  labeltop: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12,
  },
  left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4,
  },
  inputfield: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12,
  },
  iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 3,
    left: 2,
    overflow: "visible",
  },
  inputvaluewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 2,
    paddingHorizontal: 4,
    borderWidth: 0,
  },
  inputvalue: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(145, 155, 173, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
    borderWidth: 0,
    borderColor: "Tansparent",
  },
  inputpassword: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 4,
  },
  _labeltop: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    columnGap: 12,
  },
  _left: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  _label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _right: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    columnGap: 4,
  },
  _inputfield: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12,
  },
  _iconleft: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  _vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  _inputvaluewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 2,
    paddingHorizontal: 4,
  },
  _inputvalue: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(145, 155, 173, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
  },
  remembermewrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 24,
  },
  checkboxrememberme: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  __vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible",
  },
  __label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  linkforgotpassword: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  buttonlogin: {
    alignSelf: "stretch",
    flexShrink: 0,
    backgroundColor: "rgba(9, 17, 31, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 12,
  },
  textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
  },
  text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  divider: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24,
  },
  linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0,
  },
  line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible",
  },
  textwrapperD: {
    flexShrink: 0,
    paddingTop: 2,
    paddingBottom: 6,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 8,
  },
  textD: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _linewrapper: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 0,
  },
  _line: {
    alignSelf: "stretch",
    flexShrink: 0,
    minHeight: 0.001,
    overflow: "visible",
  },

  buttonsocialwrapper: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 24,
    paddingVertical: 0,
    paddingHorizontal: 24,
  },
  buttongoogle: {
    flexShrink: 0,
    paddingLeft: 14,
    paddingRight: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    paddingVertical: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12,
  },

  ___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 4,
    bottom: 12,
    left: 2,
    overflow: "visible",
  },
  buttonfacebook: {
    flexShrink: 0,
    paddingLeft: 14,
    paddingRight: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    paddingVertical: 10,
    borderWidth: 1,
    borderColor: "rgba(225, 229, 235, 1)",
    borderRadius: 12,
  },
  g10: {
    position: "absolute",
    flexShrink: 0,
    top: 20,
    height: 20,
    width: 20,
  },

  _textwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
  },
  _text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  iconleftG: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  vectorG: {
    position: "absolute",
    flexShrink: 0,
    top: 8,
    right: 1,
    bottom: 3,
    left: 10,
    overflow: "visible",
  },
  _vectorG: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 4,
    bottom: 1,
    left: 2,
    overflow: "visible",
  },
  __vectorG: {
    position: "absolute",
    flexShrink: 0,
    top: 6,
    right: 15,
    bottom: 6,
    left: 1,
    overflow: "visible",
  },
  ___vectorG: {
    position: "absolute",
    flexShrink: 0,
    top: 1,
    right: 4,
    bottom: 12,
    left: 2,
    overflow: "visible",
  },
  textwrapperG: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
  },
  textG: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _iconleftF: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _textwrapperF: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
  },
  _textF: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ctasignupwrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center",
    columnGap: 4,
  },
  donthaveanaccount: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 20,
  },
  linksignup: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
});
