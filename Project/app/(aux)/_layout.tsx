import { StyleSheet, Text, Touchable, View } from 'react-native'
import React from 'react'
import { Slot, Stack, router } from 'expo-router'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { AntDesign } from '@expo/vector-icons';

const AuxLayout = () => {
  return (
   <Stack
   screenOptions={{
    headerLeft : ()=><TouchableOpacity onPress={()=>router.back()}><AntDesign name="arrowleft" size={24} color="black" /></TouchableOpacity>
   }}
   ></Stack>
  )
}

export default AuxLayout

const styles = StyleSheet.create({})