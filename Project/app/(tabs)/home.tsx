import {
  Button,
  Dimensions,
  ImageBackground,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  Touchable,
  View,
  useWindowDimensions,
} from "react-native";
import React, { useEffect, useState } from "react";
import { service } from "../../service";
import { useDispatch, useSelector } from "react-redux";
import { TouchableOpacity } from "react-native-gesture-handler";
import { router } from "expo-router";
import { Circle, Path, Svg } from "react-native-svg";
import CardHome from "../../components/CardHome";
import CardImage from "../../components/CardImage";

const getRandomImage = () => {
  const width = 116; // Set your desired width
  const height = 154; // Set your desired height
  const accessKey = 'lUmNmuKO4wv_ex0r6QGcieKO7GB5qmhNiW4G-b0tVGs'; // Replace with your Unsplash access key
  const randomImageId = Math.floor(Math.random() * 1000) + 1; // Generate a random image ID

  return `https://source.unsplash.com/${width}x${height}/?sig=${randomImageId}&client_id=${accessKey}`;
};

const Home = () => {
  const windowWidth = Dimensions.get("window").width;
  const { width } = useWindowDimensions();
  const [showModal, setShowModal] = useState(false);
  const [firstSixElements, setFirstSixElements] = useState<any[]>([]);
  const [firstEigthElements, setFirstEigthElements] = useState<any[]>([]);
  const products = useSelector((state) => state.product.products);
  console.log(products);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log("init home");
  }, []);
  useEffect(() => {
    const fetchData = async () => {
      const { data } = await service.products.productsList();
      dispatch({ type: "SET_PRODUCTS", payload: data.paginatedResult });
      //console.log(data);
      const parsedArray = data.paginatedResult.map((item) => {
        // Assuming 'value' property is a stringified JSON
        const parsedValue =
          item.response && item.response.value
            ? JSON.parse(item.response.value)
            : null;

        return {
          ...item,
          response: {
            ...item.response,
            value: parsedValue,
          },
        };
      });
      setFirstSixElements(parsedArray.slice(0, 6));
      console.log(firstSixElements);
    };
    const fetchDataType = async () => {
      const { data } = await service.types.typesList();
      dispatch({ type: "SET_TYPES", payload: data.paginatedResult });
      //console.log(data);
      const parsedArray = data.paginatedResult.map((item) => {
        // Assuming 'value' property is a stringified JSON
        const parsedValue =
          item.response && item.response.value
            ? JSON.parse(item.response.value)
            : null;

        return {
          ...item,
          response: {
            ...item.response,
            value: parsedValue,
          },
        };
      });
      setFirstEigthElements(parsedArray.slice(0, 8));
      console.log(firstEigthElements);
    };
    fetchData();
    fetchDataType();
  }, []);

  return (
    <ScrollView>
      <View style={styles.home}>
        {/* Vigma RN:: can be replaced with <Navtop type={"brand"} screen={"mobile"} /> */}

        <View style={styles.banner}>
          {/* Vigma RN:: can be replaced with <Carousel type={"landscape"} screen={"mobile"} /> */}
          <View style={styles.carousel}>
            <View style={styles.banner1}>
              <ImageBackground
                style={[styles.image, { width: width }]} // Set width to full screen
                source={require("../../assets/banner.jpg")}
              />
              <View style={styles.text}>
                <Text style={styles.christmasSale}>{`Christmas Sale`}</Text>
                <View style={styles.discount}>
                  <Text style={styles.upto}>{`Up to`}</Text>
                  <Text style={styles.myVar}>{`70%`}</Text>
                </View>
                <View style={styles.button}>
                  <View style={styles._text}>
                    <Text style={styles.shopnow}>{`Shop now`}</Text>
                  </View>
                  {/* Vigma RN:: can be replaced with <IconsOutlineArrow_forward /> */}
                  <View style={styles.iconsOutlineArrow_forward}>
                    <View style={styles.boundingbox} />
                    <Svg
                      style={styles.vector}
                      width="8"
                      height="7"
                      viewBox="0 0 8 7"
                      fill="none"
                    >
                      <Path
                        d="M3.70835 6.74755C3.63196 6.67116 3.59203 6.57394 3.58856 6.45589C3.58509 6.33783 3.62155 6.24061 3.69794 6.16422L5.7396 4.12255H1.08335C0.965298 4.12255 0.86634 4.08262 0.786479 4.00276C0.706618 3.9229 0.666687 3.82394 0.666687 3.70589C0.666687 3.58783 0.706618 3.48887 0.786479 3.40901C0.86634 3.32915 0.965298 3.28922 1.08335 3.28922H5.7396L3.69794 1.24755C3.62155 1.17116 3.58509 1.07394 3.58856 0.955887C3.59203 0.837831 3.63196 0.740609 3.70835 0.66422C3.78474 0.587831 3.88196 0.549637 4.00002 0.549637C4.11808 0.549637 4.2153 0.587831 4.29169 0.66422L7.04169 3.41422C7.08335 3.44894 7.11287 3.49235 7.13023 3.54443C7.14759 3.59651 7.15627 3.65033 7.15627 3.70589C7.15627 3.76144 7.14759 3.81353 7.13023 3.86214C7.11287 3.91075 7.08335 3.95589 7.04169 3.99755L4.29169 6.74755C4.2153 6.82394 4.11808 6.86214 4.00002 6.86214C3.88196 6.86214 3.78474 6.82394 3.70835 6.74755Z"
                        fill="#09111F"
                      />
                    </Svg>
                  </View>
                </View>
              </View>
            </View>
            {/* Vigma RN:: can be replaced with <Pagination size={"small"} /> */}
            <View style={styles.pagination}>
              {/* Vigma RN:: can be replaced with <Dot1 size={"small"} state={"active"} /> */}
              <View style={styles.dot1}>
                <Svg
                  style={styles.dot}
                  width="8"
                  height="8"
                  viewBox="0 0 8 8"
                  fill="none"
                >
                  <Circle
                    cx="4"
                    cy="4"
                    r="4"
                    transform="rotate(-90 4 4)"
                    fill="#09111F"
                  />
                </Svg>
              </View>
              {/* Vigma RN:: can be replaced with <Dot2 size={"small"} state={"default"} /> */}
              <View style={styles.dot2}>
                <Svg
                  style={styles._dot}
                  width="8"
                  height="8"
                  viewBox="0 0 8 8"
                  fill="none"
                >
                  <Circle
                    cx="4"
                    cy="4"
                    r="4"
                    transform="rotate(-90 4 4)"
                    fill="white"
                  />
                </Svg>
              </View>
              {/* Vigma RN:: can be replaced with <Dot3 size={"small"} state={"default"} /> */}
              <View style={styles.dot3}>
                <Svg
                  style={styles.__dot}
                  width="8"
                  height="8"
                  viewBox="0 0 8 8"
                  fill="none"
                >
                  <Circle
                    cx="4"
                    cy="4"
                    r="4"
                    transform="rotate(-90 4 4)"
                    fill="white"
                  />
                </Svg>
              </View>
              {/* Vigma RN:: can be replaced with <Dot4 size={"small"} state={"default"} /> */}
              <View style={styles.dot4}>
                <Svg
                  style={styles.___dot}
                  width="8"
                  height="8"
                  viewBox="0 0 8 8"
                  fill="none"
                >
                  <Circle
                    cx="4"
                    cy="4"
                    r="4"
                    transform="rotate(-90 4 4)"
                    fill="white"
                  />
                </Svg>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.roomideas}>
          <View style={styles.title}>
            <Text style={styles._roomideas}>{`Room ideas`}</Text>
            {/* Vigma RN:: can be replaced with <Buttonviewmore color={"dark"} size={"small"} state={"default"} /> */}
            <View style={styles.buttonviewmore}>
              <View style={styles.textwrapper}>
                <Text style={styles.__text}>{`View more`}</Text>
              </View>
              {/* Vigma RN:: can be replaced with <Icon /> */}
              <View style={styles.icon}>
                <View style={styles.__boundingbox} />
                <Svg
                  style={styles.__vector}
                  width="5"
                  height="8"
                  viewBox="0 0 5 8"
                  fill="none"
                >
                  <Path
                    d="M0.466659 7.53332C0.344436 7.4111 0.283325 7.25555 0.283325 7.06666C0.283325 6.87777 0.344436 6.72221 0.466659 6.59999L3.06666 3.99999L0.466659 1.39999C0.344436 1.27777 0.283325 1.12221 0.283325 0.933325C0.283325 0.744436 0.344436 0.588881 0.466659 0.466659C0.588881 0.344436 0.744436 0.283325 0.933325 0.283325C1.12221 0.283325 1.27777 0.344436 1.39999 0.466659L4.46666 3.53332C4.53333 3.59999 4.58055 3.67221 4.60833 3.74999C4.6361 3.82777 4.64999 3.9111 4.64999 3.99999C4.64999 4.08888 4.6361 4.17221 4.60833 4.24999C4.58055 4.32777 4.53333 4.39999 4.46666 4.46666L1.39999 7.53332C1.27777 7.65555 1.12221 7.71666 0.933325 7.71666C0.744436 7.71666 0.588881 7.65555 0.466659 7.53332Z"
                    fill="#09111F"
                  />
                </Svg>
              </View>
            </View>
          </View>
          <View style={styles.scrollablecontent}>
            {/* Vigma RN:: can be replaced with <Card type={"fullImage"} orientation={"horizontal"} size={"small"} /> */}
            <ImageBackground
              style={styles.card}
              source={{
                uri: /* dummy image */ "https://dummyimage.com/256x154/000/fff.jpg",
              }}
            >
              <View style={styles.___text}>
                <Text style={styles._title}>
                  {`Stylish home for young family`}
                </Text>
                <Text style={styles.subtitle}>{`12 Items`}</Text>
              </View>
            </ImageBackground>
            {/* Vigma RN:: can be replaced with <_card type={"fullImage"} orientation={"horizontal"} size={"small"} /> */}
            <ImageBackground
              style={styles._card}
              source={{
                uri: /* dummy image */ "https://dummyimage.com/256x154/000/fff.jpg",
              }}
            >
              <View style={styles.____text}>
                <Text style={styles.__title}>{`The beauty of simplicity`}</Text>
                <Text style={styles._subtitle}>{`3 Items`}</Text>
              </View>
            </ImageBackground>
            {/* Vigma RN:: can be replaced with <__card type={"fullImage"} orientation={"horizontal"} size={"small"} /> */}
            <ImageBackground
              style={styles.__card}
              source={{
                uri: /* dummy image */ "https://dummyimage.com/256x154/000/fff.jpg",
              }}
            >
              <View style={styles._____text}>
                <Text style={styles.___title}>
                  {`Sleep tight in traditional style`}
                </Text>
                <Text style={styles.__subtitle}>{`8 Items`}</Text>
              </View>
            </ImageBackground>
            {/* Vigma RN:: can be replaced with <___card type={"fullImage"} orientation={"horizontal"} size={"small"} /> */}
            <ImageBackground
              style={styles.___card}
              source={{
                uri: /* dummy image */ "https://dummyimage.com/256x154/000/fff.jpg",
              }}
            >
              <View style={styles.______text}>
                <Text style={styles.____title}>
                  {`Traditional yet luxurious design`}
                </Text>
                <Text style={styles.___subtitle}>{`6 Items`}</Text>
              </View>
            </ImageBackground>
          </View>
        </View>
        <View style={styles.shopbyroom}>
          <View style={styles._____title}>
            <Text style={styles._shopbyroom}>{`Shop by room`}</Text>
          </View>
          <View style={styles._scrollablecontent}>
          {firstEigthElements.map((element) => (
            <CardImage element={element}/>
             ))}
            {/* Vigma RN:: can be replaced with <_____card type={"fullImage"} orientation={"vertical"} size={"small"} /> */}
           
          </View>
        </View>
        <View style={styles.recommendedforyou}>
          <View style={styles.______________title}>
            <Text
              style={styles._recommendedforyou}
            >{`Recommended for you`}</Text>
          </View>
          <View style={styles.items}>
            {/* Vigma RN:: can be replaced with <____________card type={"product"} orientation={"vertical"} size={"small"} /> */}
            {firstSixElements.map((element) => (
        <CardHome key={element.id} element={element} />
      ))}
            {/* Vigma RN:: can be replaced with <_____________card type={"product"} orientation={"vertical"} size={"small"} /> */}

            {/* Vigma RN:: can be replaced with <______________card type={"product"} orientation={"vertical"} size={"small"} /> */}
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  home: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 88,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  navtop: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingLeft: 24,
    paddingRight: 14,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
  },
  logo: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 6,
  },
  _logo: {
    flexShrink: 0,
    height: 24,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 6,
  },
  logomark: {
    flexShrink: 0,
    height: 24,
    width: 35,
  },
  mAYNOOTH: {
    flexShrink: 0,
    width: 115,
    height: 15,
    overflow: "visible",
  },
  notification: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineNotifications: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  badge: {
    position: "absolute",
    flexShrink: 0,
    top: 7,
    height: 6,
    left: 27,
    width: 6,
    alignItems: "flex-start",
    rowGap: 0,
  },
  dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  banner: {
    flexShrink: 0,
    width: "100%",
    alignItems: "center",
    rowGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 24,
    height: 450,
  },
  carousel: {
    width: "100%",
    flexGrow: 1,
    height: 188,
    alignItems: "center",
    rowGap: 12,
    borderRadius: 12,
  },

  banner1: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  image: {
    position: "absolute",
    height: "100%",
    borderRadius: 12,
    alignContent: "center",
  },

  text: {
    position: "absolute",
    flexShrink: 0,
    top: 90,
    height: 109,
    left: 29,
    width: 129,
    alignItems: "flex-start",
    rowGap: 10,
    borderRadius: 12,
  },
  christmasSale: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 18,
    fontWeight: "900",
    letterSpacing: 0,
    lineHeight: 16.941177368164062,
  },
  discount: {
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 4,
  },
  upto: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 11.29411792755127,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 11.29411792755127,
  },
  myVar: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 30.117647171020508,
    fontWeight: "900",
    letterSpacing: 0,
    lineHeight: 30.117647171020508,
  },
  button: {
    flexShrink: 0,
    width: 66,
    paddingLeft: 8,
    paddingRight: 6,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    columnGap: 2,
    paddingVertical: 4,
    borderRadius: 4,
  },
  _text: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 1.4117647409439087,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  shopnow: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 8.470588684082031,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 9.411765098571777,
  },
  iconsOutlineArrow_forward: {
    flexShrink: 0,
    height: 10,
    width: 10,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  _vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  pagination: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    left: "50%", // Set left to 50%

    flexDirection: "row",
    alignItems: "center", // Center items vertically within pagination
    columnGap: 12,
  },
  dot1: {
    flexShrink: 0,
    height: 8,
    width: 8,
    transform: [
      {
        rotateZ: "-90.00deg",
      },
    ],
    alignItems: "flex-start",
    rowGap: 0,
  },
  _dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  dot2: {
    flexShrink: 0,
    height: 8,
    width: 8,
    transform: [
      {
        rotateZ: "-90.00deg",
      },
    ],
    alignItems: "flex-start",
    rowGap: 0,
  },
  __dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  dot3: {
    flexShrink: 0,
    height: 8,
    width: 8,
    transform: [
      {
        rotateZ: "-90.00deg",
      },
    ],
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  dot4: {
    flexShrink: 0,
    height: 8,
    width: 8,
    transform: [
      {
        rotateZ: "-90.00deg",
      },
    ],
    alignItems: "flex-start",
    rowGap: 0,
  },
  ____dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  roomideas: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 24,
    paddingBottom: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24,
  },
  title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-end",
    columnGap: 12,
  },
  _roomideas: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  buttonviewmore: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 0,
    paddingVertical: 1,
    paddingHorizontal: 0,
  },
  textwrapper: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingHorizontal: 0,
  },
  __text: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  __vector: {
    position: "absolute",
    flexShrink: 0,
    top: 4,
    right: 5,
    bottom: 4,
    left: 6,
    overflow: "visible",
  },
  scrollablecontent: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 24,
  },
  card: {
    flexShrink: 0,
    height: 154,
    width: 256,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ___text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  _title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _card: {
    flexShrink: 0,
    height: 154,
    width: 256,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ____text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  __title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  _subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
  },
  __card: {
    flexShrink: 0,
    height: 154,
    width: 256,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _____text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ___title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  __subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ___card: {
    flexShrink: 0,
    height: 154,
    width: 256,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ______text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ____title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  ___subtitle: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
  },
  shopbyroom: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 24,
    paddingBottom: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24,
  },
  _____title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
  },
  _shopbyroom: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  _scrollablecontent: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 24,
  },
  ____card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _______text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ______title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  ___vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible",
  },
  _____card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  _______title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  __icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible",
  },
  ______card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  __buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ___icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _____boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _______card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  __________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  _________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ___buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ____icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ________card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ___________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  __________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ____buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _____icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _______boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _________card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ____________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ___________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _____buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ______icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  __________card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _____________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  ____________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ______buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _______icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ___________card: {
    flexShrink: 0,
    height: 154,
    width: 116,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ______________text: {
    alignSelf: "stretch",
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 0,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 12,
  },
  _____________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(255, 255, 255, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _______buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    bottom: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  recommendedforyou: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 24,
    paddingBottom: 0,
    alignItems: "flex-start",
    rowGap: 12,
    paddingHorizontal: 24,
  },
  ______________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
  },
  _recommendedforyou: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 16,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 24,
  },
  items: {
    alignSelf: "stretch",
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 24,
    flexWrap: "wrap",
  },
  col: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 24,
  },
  ____________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
    width: "47%",
    margin: 10, // Set width to 50%
  },
  _image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  _______________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  _badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _____vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  _label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ____________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    
  },
  ______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  _____________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  __image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  _cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  ________________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  _currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  _initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  __badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  __label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  _iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _____________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _______vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  ___label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  __________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ______________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  ______________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ___image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  __cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  _________________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  __price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  __currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  __initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  __initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  ___badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  ____label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  __ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  __iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _______________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  _____label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  __________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ___________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  __________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  _col: {
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
    alignItems: "flex-start",
    rowGap: 24,
  },
  _______________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ____image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  ___cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  __________________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ___price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  ___currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ___initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  ___initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  ____badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  ______label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ___ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  ___iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ___________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  _______label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ___________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ____________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ____________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  ________________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  _____image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  ____cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  ___________________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  ____price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  ____currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ____initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  ____initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  _____badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  ________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ____ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  ____iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _____________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  _________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  ____________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  _____________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ____________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ______________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  _________________card: {
    alignSelf: "stretch",
    flexShrink: 0,
    alignItems: "flex-start",
    rowGap: 0,
    borderRadius: 12,
  },
  ______image: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 144,
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  _____cardbody: {
    alignSelf: "stretch",
    flexShrink: 0,
    borderTopWidth: 0,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    alignItems: "flex-start",
    rowGap: 2,
    paddingVertical: 4,
    paddingHorizontal: 8,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  ____________________title: {
    alignSelf: "stretch",
    flexShrink: 0,
    height: 20,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  _____price: {
    alignSelf: "stretch",
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    alignItems: "flex-start",
    rowGap: 0,
    paddingHorizontal: 0,
  },
  _____currentprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _____initialpricewrapper: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    columnGap: 4,
  },
  _____initialprice: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "400",
    letterSpacing: 0,
    lineHeight: 16,
    textDecorationLine: "line-through",
  },
  ______badge: {
    flexShrink: 0,
    backgroundColor: "rgba(215, 246, 228, 1)",
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 0,
    paddingVertical: 0,
    paddingHorizontal: 4,
    borderRadius: 4,
  },
  __________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(8, 199, 84, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _____ratingProduct: {
    flexShrink: 0,
    paddingTop: 0,
    paddingBottom: 4,
    flexDirection: "row",
    alignItems: "flex-start",
    columnGap: 2,
    paddingHorizontal: 0,
  },
  _____iconsFilledStar: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _____________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _______________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  ___________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(76, 89, 112, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 12,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 16,
  },
  _____________buttonfloating: {
    position: "absolute",
    flexShrink: 0,
    top: 12,
    right: 12,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 2,
    padding: 6,
    borderRadius: 9999,
  },
  ______________icon: {
    flexShrink: 0,
    height: 16,
    width: 16,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ______________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 1,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  navbottom: {
    position: "absolute",
    flexShrink: 0,
    bottom: 0,
    left: 0,
    right: 0,
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    backgroundColor: "rgba(255, 255, 255, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    columnGap: 0,
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderColor: "rgba(225, 229, 235, 1)",
  },
  item1: {
    flexShrink: 0,
    paddingLeft: 10,
    paddingRight: 14,
    backgroundColor: "rgba(240, 242, 245, 1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    paddingVertical: 10,
    borderRadius: 9999,
  },
  iconfilled: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _______________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 2,
    left: 3,
    overflow: "visible",
  },
  ____________label: {
    flexShrink: 0,
    textAlign: "left",
    color: "rgba(9, 17, 31, 1)",
    fontFamily: "Satoshi Variable",
    fontSize: 14,
    fontWeight: "700",
    letterSpacing: 0,
    lineHeight: 20,
  },
  item2: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  _______________icon: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ________________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  __________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 3,
    left: 2,
    overflow: "visible",
  },
  item3: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineShopping_cart: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _________________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ___________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 2,
    right: 3,
    bottom: 2,
    left: 1,
    overflow: "visible",
  },
  _______badge: {
    position: "absolute",
    flexShrink: 0,
    top: 7,
    height: 6,
    left: 27,
    width: 6,
    alignItems: "flex-start",
    rowGap: 0,
  },
  _____dot: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    overflow: "visible",
  },
  item4: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlineFavorite: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  __________________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  ____________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 2,
    bottom: 2,
    left: 2,
    overflow: "visible",
  },
  item5: {
    flexShrink: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    columnGap: 4,
    padding: 10,
    borderRadius: 9999,
  },
  iconsOutlinePerson: {
    flexShrink: 0,
    height: 20,
    width: 20,
    alignItems: "flex-start",
    rowGap: 0,
  },
  ___________________________boundingbox: {
    position: "absolute",
    flexShrink: 0,
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: "rgba(217, 217, 217, 1)",
  },
  _____________________vector: {
    position: "absolute",
    flexShrink: 0,
    top: 3,
    right: 3,
    bottom: 3,
    left: 3,
    overflow: "visible",
  },
});
